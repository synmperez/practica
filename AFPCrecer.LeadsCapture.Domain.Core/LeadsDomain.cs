﻿using System;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;
using AFPCrecer.LeadsCapture.Cross.Common;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace AFPCrecer.LeadsCapture.Domain.Core
{
    public class LeadsDomain:ILeadsDomain
    {
        private readonly IRunStoreProcedureRepository _runStoreProcedureRepository;
        private readonly IEncryptWizard _encryptWizard;
        private readonly IConfiguration _configuration;
        public LeadsDomain(IConfiguration configuration,IRunStoreProcedureRepository runStoreProcedureRepository,IEncryptWizard encryptWizard)
        {
            _runStoreProcedureRepository = runStoreProcedureRepository;
            _encryptWizard = encryptWizard;
            _configuration = configuration;
        }

        public async Task<object> RegistryLeadAsync(Leads lead)
        {
            string[,] parameters = new string[,] { { "@NOMBRE", decrypt(lead.Name) }, { "@APELLIDO", decrypt(lead.Lastname) }, { "@EMAIL", decrypt(lead.Mail) }, { "@NUMERO_TELEFONO", decrypt(lead.Phone) }, { "@DUI",decrypt(lead.DUI) }};
            DataTable dt = await _runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("APV_CAL_PRC_REGISTRO_LEADS_TR", parameters);
            DataRow row = dt.Rows[0];
            if (row != null)
            {
                string Id = encrypt(row.ItemArray[0].ToString());
                string Name = encrypt(row.ItemArray[1].ToString());
                var result = new { Id, Name};
                return result;

            }
                return null;
        }

        public string decrypt(string value)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(_configuration["DecryptOnBackendKey"]));

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

           return _encryptWizard.Decrypt(value, key, iv);
        }

        public string encrypt(string value)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(_configuration["EncryptOnBackendKey"]));

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return _encryptWizard.Encrypt(value, key, iv);
        }

    }
}

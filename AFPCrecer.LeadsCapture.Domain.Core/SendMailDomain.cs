﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Domain.Core
{
    public class SendMailDomain: ISendMailDomain
    {
        private readonly IRunStoreProcedureRepository _runStoreProcedureRepository;
        private readonly IEncryptWizard _encryptWizard;
        private readonly IConfiguration _configuration;

        public SendMailDomain(IConfiguration configuration, IRunStoreProcedureRepository runStoreProcedureRepository, IEncryptWizard encryptWizard)
        {
            _runStoreProcedureRepository = runStoreProcedureRepository;
            _encryptWizard = encryptWizard;
            _configuration = configuration;
        }
        public async Task<List<string>> GetMailInfo(MailRequest mail)
        {
            var mailDecrypt = new MailRequest();
            string[,] parameters = new string[,] { { "@LEAD_USER", decrypt(mail.IdUser) }, { "@SURVEY", decrypt(mail.IdCampaign) }};
            DataTable dt = await _runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("APV_CAL_PRC_RESULTADOS_DEL_LEAD_TR", parameters);
            string[,] parameters2 = new string[,] { { "@LEAD_USER", decrypt(mail.IdUser) }, { "@SURVEY", decrypt(mail.IdCampaign) }, { "@OPTION", "1" } };
            DataTable dt2 = await _runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("APV_CAL_PRC_RESULTADOS_DEL_LEAD_TR", parameters2);

            if(dt2 ==null) return null;
            if (dt2.Rows.Count == 1)
            {
                if (dt2.Columns.Count < 3)
                {
                    return null;
                }
            }

            string pdfStr =await CreateStringPDF(Convert.ToInt32(decrypt(mail.IdCampaign)),dt2);
            
            DataRow row = dt.Rows[0];

            if (row != null)
            {
                if (row["RESULT_CODE"].ToString() == "200")
                {
                    string email = row["EMAIL"].ToString();
                    string pdfFormat = pdfStr;
                    string fullName = row["NOMBRE_COMPLETO"].ToString();
                    string subject = row["SUBJECT"].ToString();
                    string message = row["MESSAGE"].ToString();
                    string from_email = row["DE_EMAIL"].ToString();
                    string from_name = row["DE_NOMBRE"].ToString();
                    List<string> result = new List<string>() { email, pdfFormat, fullName, subject, message, from_email, from_name };
                    return result;
                }
            }
            return null;
        }

        public string decrypt(string value)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(_configuration["DecryptOnBackendKey"]));

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return _encryptWizard.Decrypt(value, key, iv);
        }

        public async Task<string> CreateStringPDF(int survey,DataTable dt)
        {
            switch(survey)
            {
                case 1:
                    return await InvestorPDFFormatAsync(dt);
                case 2:
                    return await FinantialHealthPDFFormatAsync(dt);
                case 3:
                    return await DreamsEstimationPDFFormatAsync(dt);
            }
            return "";
        }
        public async Task<string> InvestorPDFFormatAsync(DataTable table)
        {
            HttpClient client = new HttpClient();
            string html = await client.GetStringAsync(_configuration["StaticURL"] + "/InvestorMailFormat1.html");
            html = html.Replace("{0}", table.Rows[0].ItemArray[1].ToString());
            html = html.Replace("{1}", table.Rows[1].ItemArray[1].ToString());
            html = html.Replace("{2}", table.Rows[2].ItemArray[1].ToString());
            html = html.Replace("{3}", table.Rows[3].ItemArray[1].ToString());
            html = html.Replace("{4}", table.Rows[4].ItemArray[1].ToString());
            html = html.Replace("{5}", table.Rows[5].ItemArray[1].ToString());
            html = html.Replace("{6}", table.Rows[6].ItemArray[1].ToString());
            html = html.Replace("{7}", table.Rows[7].ItemArray[1].ToString());
            html = html.Replace("{INV_PROFILE}", table.Rows[8].ItemArray[1].ToString());
            html = html.Replace("{TYPE}", table.Rows[8].ItemArray[1].ToString());
            html = html.Replace("{INV_PROFILE_DESC}", table.Rows[9].ItemArray[1].ToString());
            html = html.Replace("{FONT_URL}", _configuration["StaticURL"]);
            html = html.Replace("{IMG_URL}", _configuration["StaticURL"]);
            return html;
        }
        public async Task<string> FinantialHealthPDFFormatAsync(DataTable table)
        {
            HttpClient client = new HttpClient();
            string html = await client.GetStringAsync(_configuration["StaticURL"] + "/FinantialHealthFormat.html");
         
            html = html.Replace("{0}", table.Rows[0].ItemArray[1].ToString());
            html = html.Replace("{1}", table.Rows[1].ItemArray[1].ToString());
            html = html.Replace("{2}", table.Rows[2].ItemArray[1].ToString());
            html = html.Replace("{3}", table.Rows[3].ItemArray[1].ToString());
            html = html.Replace("{4}", table.Rows[4].ItemArray[1].ToString());
            html = html.Replace("{5}", table.Rows[5].ItemArray[1].ToString());
            html = html.Replace("{6}", table.Rows[6].ItemArray[1].ToString());
            html = html.Replace("{7}", table.Rows[7].ItemArray[1].ToString());
            html = html.Replace("{8}", table.Rows[8].ItemArray[1].ToString());
            html = html.Replace("{9}", table.Rows[9].ItemArray[1].ToString());
            html = html.Replace("{10}", table.Rows[10].ItemArray[1].ToString());
            html = html.Replace("{11}", table.Rows[11].ItemArray[1].ToString());
            html = html.Replace("{12}", table.Rows[12].ItemArray[1].ToString());
            html = html.Replace("{PROFILE}", table.Rows[13].ItemArray[1].ToString());
            html = html.Replace("{PROFILE_DESC}", table.Rows[14].ItemArray[1].ToString());
            html = html.Replace("{FONT_URL}", _configuration["StaticURL"]);
            html = html.Replace("{IMG_URL}", _configuration["StaticURL"]);
            return html;
        }
        public async Task<string> DreamsEstimationPDFFormatAsync(DataTable table)
        {
            DataRow row = table.Rows[0];
            double Gap_savings = Convert.ToDouble(row["GAP_AHORROS"]);
            string Profile_dreams;
            string Comp_dreams;
            double gap_total;
            if(Gap_savings<0)
            {
                Profile_dreams = "Felicitaciones! Puedes lograr tu meta";
                Comp_dreams = "un excedente";
                gap_total = Math.Abs(Gap_savings);
            }
            else
            {
                Profile_dreams = "'Debes ajustar tu plan de ahorro";
                Comp_dreams = "'una brecha";
                gap_total = Convert.ToDouble(row["GAP_AHORROS"]);
            }
            NumberFormatInfo formatNum = new NumberFormatInfo();
            formatNum.CurrencySymbol = "$";
            double total_Dreams =Convert.ToDouble(row["AHORRO_TOTAL"]);
            double montlhy_salary_dreams = Convert.ToDouble(row["INGRESOS_MENSUALES"]);
            double aditional_salary_dreams = Convert.ToDouble(row["INGRESOS_ADICIONALES"]);
            double Saving_Goal_dreams = Convert.ToDouble(row["META_AHORRO"]);
            double Saving_Actual_dreams = Convert.ToDouble(row["AHORROS_ACTUALES"]);
            double Saving_Estimate_dreams = Convert.ToDouble(row["AHORRO_ESTIMADO"]);

            HttpClient client = new HttpClient();
            string html = await client.GetStringAsync(_configuration["StaticURL"] + "/DreamsEstimationFormat.html");

            html = html.Replace("{DREAM_TYPE}", row["SUENO"].ToString());
            html = html.Replace("{MONTHLY_INCOME}", montlhy_salary_dreams.ToString("C", formatNum));
            html = html.Replace("{ADITIONAL_INCOME}", aditional_salary_dreams.ToString("C", formatNum));
            html = html.Replace("{ADITIONAL_INCOME_PERIOD}",TimeEstimate(Convert.ToInt32(row["PERIOCIDAD_INGRESOS_ADICIONALES"])));
            html = html.Replace("{GOALS_SAVING}", Saving_Goal_dreams.ToString("C", formatNum));
            html = html.Replace("{ACTUAL_SAVING}", Saving_Actual_dreams.ToString("C", formatNum));
            html = html.Replace("{PERIOD_SAVING}", Saving_Estimate_dreams.ToString("C", formatNum));
            html = html.Replace("{INPUT_PERIOD}", TimeEstimate(Convert.ToInt32(row["PERIOCIDAD_APORTE"])));
            html = html.Replace("{DREAM_YEARS}", row["ANOS_AHORRO"].ToString());
            html = html.Replace("{TOTAL_SAVINGS}", total_Dreams.ToString("C", formatNum));
            html = html.Replace("{GAP_NATURE}", Comp_dreams);
            html = html.Replace("{PROFILE}", Profile_dreams);
            html = html.Replace("{GAP_SAVINGS}", gap_total.ToString("C",formatNum));
            html = html.Replace("{FONT_URL}", _configuration["StaticURL"]);
            html = html.Replace("{IMG_URL}", _configuration["StaticURL"]);
            return html;
        }

        public string TimeEstimate(int typeTime)
        {
            switch(typeTime)
            {
                case 24:
                    return "Quincenal";
                case 12:
                    return "Mensual";
                case 4:
                    return "Trimestral";
                case 2:
                    return "Semestral";
                case 1:
                    return "Anual";
                default:
                    return typeTime.ToString();
            }
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Domain.Core
{
    public class DreamsEstimateDomain: IDreamsEstimationDomain
    {
        private readonly IRunStoreProcedureRepository _runStoreProcedureRepository;
        private readonly IEncryptWizard _encryptWizard;
        private readonly IConfiguration _configuration;
        public DreamsEstimateDomain(IRunStoreProcedureRepository runStoreProcedureRepository,IConfiguration configuration, IEncryptWizard encryptWizard)
        {
            _runStoreProcedureRepository = runStoreProcedureRepository;
            _encryptWizard = encryptWizard;
            _configuration = configuration;
        }

        public async Task<object> RegistryDreamsEstimateAsync(DreamEstimate dreamEstimate)
        {
            string[,] parameters;
            if (dreamEstimate.Other == null)
            {
                 parameters = new string[,] { { "@ID_LEAD", decrypt(dreamEstimate.IdUser) }, { "@INGRESOS_MENSUALES", decrypt(dreamEstimate.MontlyIncome) }, { "@INGRESOS_ADICIONALES", decrypt(dreamEstimate.AditionalIncome) }, { "@PERIOCIDAD_INGRESOS_ADICIONALES", decrypt(dreamEstimate.PeriodAdionaltIncome) }, { "@META_AHORRO",decrypt(dreamEstimate.SavingsGoal) }, { "@AHORROS_ACTUALES", decrypt(dreamEstimate.ActualMoney) },
                                                    { "@APORTE_PARA_AHORRAR",decrypt(dreamEstimate.ExpectationSaving)},{"@PERIOCIDAD_APORTES",decrypt(dreamEstimate.PeriodInputSaving)},{"@ANOS_META",decrypt(dreamEstimate.DreamTimeYears) },{"@TIPO_SUENO_OPCION",decrypt(dreamEstimate.DreamType) }};

            }
            else
            {
                 parameters = new string[,] { { "@ID_LEAD", decrypt(dreamEstimate.IdUser) }, { "@INGRESOS_MENSUALES", decrypt(dreamEstimate.MontlyIncome) }, { "@INGRESOS_ADICIONALES", decrypt(dreamEstimate.AditionalIncome) }, { "@PERIOCIDAD_INGRESOS_ADICIONALES", decrypt(dreamEstimate.PeriodAdionaltIncome) }, { "@META_AHORRO",decrypt(dreamEstimate.SavingsGoal) }, { "@AHORROS_ACTUALES", decrypt(dreamEstimate.ActualMoney) },
                                                    { "@APORTE_PARA_AHORRAR",decrypt(dreamEstimate.ExpectationSaving)},{"@PERIOCIDAD_APORTES",decrypt(dreamEstimate.PeriodInputSaving)},{"@ANOS_META",decrypt(dreamEstimate.DreamTimeYears) },{"@TIPO_SUENO_OPCION",decrypt(dreamEstimate.DreamType) },{"@OTRO_SUENO",decrypt(dreamEstimate.Other) } };

            }

            DataTable dt = await _runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("APV_CAL_PRC_ESTIMACION_SUENOS_TR", parameters);      
            DataRow row = dt.Rows[0];
            if (row != null)
            {
                string AhorroTotal = row.ItemArray[1].ToString();
                string Brecha= row.ItemArray[2].ToString();
                var result = new { AhorroTotal, Brecha };
                return result;
            }
            return null;
        }
        public string decrypt(string value)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(_configuration["DecryptOnBackendKey"]));

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return _encryptWizard.Decrypt(value, key, iv);
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Domain.Core
{
    public class FinancialHealthDomain: IFinancialHealthDomain
    {
        private readonly IRunStoreProcedureRepository _runStoreProcedureRepository;
        private readonly IEncryptWizard _encryptWizard;
        private readonly IConfiguration _configuration;

        public FinancialHealthDomain(IRunStoreProcedureRepository runStoreProcedureRepository, IConfiguration configuration, IEncryptWizard encryptWizard)
        {
            _runStoreProcedureRepository = runStoreProcedureRepository;
            _encryptWizard = encryptWizard;
            _configuration = configuration;
        }

        public async Task<string[]> RegistryFinancialHealthStateAsync(FinancialHealth financial)
        {
            string[,] parameters = new string[,] { { "@LEAD_USER", decrypt(financial.IdUser) },{"@GENERO", decrypt(financial.Genre) },{"@RANGO_EDAD",decrypt(financial.AgeRange) },{"@SITUACION_LABORAL",decrypt(financial.Employment_Situation)}, { "@RANGO_INGRESOS_MENSUALES", decrypt(financial.IncomeRange) }, { "@PORCENTAJE_INGRESOS_AHORRADOS_ANO_ANTERIOR", decrypt(financial.IncomeSavedPercentageLastYear)}, { "@INGRESOS_EGRESOS_DETALLE", decrypt(financial.IncomeAndExpensesDetail) }, { "@COMPARACION_SITUACION_FINANCIERA",decrypt(financial.FinancialSitutationComparison) }, { "@SOBREVIVIR_SIN_PRESTAR", decrypt(financial.SurviveWithCurrentMoney) },
                                                    { "@GASTOS_INESPERADOS",decrypt(financial.UnexpectedExpenses)},{"@SITUACION_ENDEUDAMIENTO",decrypt(financial.DebtSituation)},{"@FONDOS_FINAL_DE_MES",decrypt(financial.FinalMonthlyFunds) },{"@ANALISIS_SITUACION_EN_SEMESTRE",decrypt(financial.SemesterFinancialDecisions) },{"@DECISION_ULTIMA_GRAN_COMPRA",decrypt(financial.LastPurchaseDecisions) } };
            DataTable dt = await _runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("APV_CAL_PRC_SALUD_FINANCIERA_TR", parameters);

            DataRow row = dt.Rows[0];
            if (row != null)
            {
                string HealthState = row["HEALTH_STATE"].ToString();
                string HealthTitle= row["HEALTH_TITLE"].ToString();
                string Code = row["RESULT_CODE"].ToString();
                string[] result = new string[3]{ HealthTitle, HealthState,Code };
                return result;
            }
            return null;
        }
        public string decrypt(string value)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(_configuration["DecryptOnBackendKey"]));

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return _encryptWizard.Decrypt(value, key, iv);
        }
    }
}

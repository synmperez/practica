﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Domain.Core
{
   public class InvestorDomain:IInvestorDomain
    {
        private readonly IRunStoreProcedureRepository _runStoreProcedureRepository;
        private readonly IEncryptWizard _encryptWizard;
        private readonly IConfiguration _configuration;
        public InvestorDomain(IRunStoreProcedureRepository runStoreProcedureRepository, IConfiguration configuration, IEncryptWizard encryptWizard)
        {
            _runStoreProcedureRepository = runStoreProcedureRepository;
            _encryptWizard = encryptWizard;
            _configuration = configuration;
        }

        public async Task<string[]> RegistryInvestorAsync(Investor investor)
        {
            string[,] parameters = new string[,] { { "@LEAD_USER", decrypt(investor.IdUser) }, { "@EDAD_INVERSOR", decrypt(investor.InvestorAge) }, { "@PERIOD_ANO", decrypt(investor.PeriodYear) }, { "@CONSIDERACION_OBJETIVOS", decrypt(investor.ObjectivesConsideration) }, { "@NIVEL_INVERSOR",decrypt(investor.InvestorLevel) }, { "@INVERSIONES_INVERSOR", decrypt(investor.InvestorInvestiment) },
                                                   { "@ESCENARIO_INVERSOR",decrypt(investor.InvestorStage)},{"@DESVALORIZACION_DECISION",decrypt(investor.InvestorDecision)},{"@PORCENTAJE_INVERSION",decrypt(investor.InvestorPercentage) } };
            DataTable dt = await _runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("APV_CAL_PRC_CALC_INVESTOR_TR", parameters);
            DataRow row = dt.Rows[0];
            if (row != null)
            {
                string Code = row.ItemArray[1].ToString();
                string InvestorType =row.ItemArray[0].ToString();     
                string[] result = new string[2] { InvestorType,Code};
                return result;
            }
            return null;
        }
        public string decrypt(string value)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(_configuration["DecryptOnBackendKey"]));

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return _encryptWizard.Decrypt(value, key, iv);
        }

    }
}

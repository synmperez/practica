﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Domain.Core
{
   public class PensionEstimateDomain: IPensionEstimateDomain
    {
        private readonly IRunStoreProcedureRepository _runStoreProcedureRepository;
        private readonly IEncryptWizard _encryptWizard;
        private readonly IConfiguration _configuration;
        public PensionEstimateDomain(IRunStoreProcedureRepository runStoreProcedureRepository, IConfiguration configuration, IEncryptWizard encryptWizard)
        {
            _runStoreProcedureRepository = runStoreProcedureRepository;
            _encryptWizard = encryptWizard;
            _configuration = configuration;
        }
        public async Task<string[]> EstimatePensionAsync(PensionEstimate pensionEstimate)
        {
            string[,] parameters = new string[,] { { "@LEAD_USER", decrypt(pensionEstimate.IdUser) }, { "@GENRE", decrypt(pensionEstimate.Genre) }, { "@SPP_TIME", decrypt(pensionEstimate.SppTime) }, { "@SPP_SALARY", decrypt(pensionEstimate.SppSalary) }, { "@SPP_ACTUAL_BALANCE",decrypt(pensionEstimate.SppActualBalance) }, { "@BIRTH_DATE", decrypt(pensionEstimate.BirthDate) },
                                                    { "@QUOTED_ACTUAL_TIME_SAP",decrypt(pensionEstimate.QuotedActualTimeSap)},{"@ACTUAL_SALARY",decrypt(pensionEstimate.ActualSalary)},{"@RETIREMENT_DESIRED_AGE",decrypt(pensionEstimate.RetirementDesiredAge) }, };
            DataTable dt = await _runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("AFP_LEADS_SP_CALCULATE_PENSION_TR", parameters);
            DataRow row = dt.Rows[0];
            if (row != null)
            {
                string Pension = row.ItemArray[0].ToString();
                string code = row.ItemArray[1].ToString();
                string[] result = new string[2] { Pension, code};
                return result;
            }
            return null;
        }
        public string decrypt(string value)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(_configuration["DecryptOnBackendKey"]));

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return _encryptWizard.Decrypt(value, key, iv);
        }
    }
}

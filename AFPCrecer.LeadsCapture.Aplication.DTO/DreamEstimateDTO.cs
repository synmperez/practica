﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AFPCrecer.LeadsCapture.Aplication.DTO
{
   public class DreamEstimateDTO
    {
        [Required]
        public string IdUser { get; set; }
        [Required]
        public string DreamType { get; set; }
        [Required]
        public string MontlyIncome { get; set; }
        [Required]
        public string AditionalIncome { get; set; }
        [Required]
        public string PeriodAdionaltIncome { get; set; }
        [Required]
        public string SavingsGoal { get; set; }
        [Required]
        public string ActualMoney { get; set; }
        [Required]
        public string ExpectationSaving { get; set; }
        [Required]
        public string PeriodInputSaving { get; set; }
        [Required]
        public string DreamTimeYears { get; set; }
        public string Other { get; set; }
    }
}

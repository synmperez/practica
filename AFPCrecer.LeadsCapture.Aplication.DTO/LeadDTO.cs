﻿
namespace AFPCrecer.LeadsCapture.Aplication.DTO
{
    public class LeadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Mail { get; set; }
        public string Phone { get; set; }
        public string DUI { get; set; }
    }
}

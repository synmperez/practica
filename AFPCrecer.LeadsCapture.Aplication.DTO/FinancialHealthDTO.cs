﻿
namespace AFPCrecer.LeadsCapture.Aplication.DTO
{
    public class FinancialHealthDTO
    {
        public string IdUser { get; set; }
        public string IncomeRange { get; set; }
        public string IncomeSavedPercentageLastYear { get; set; }
        public string IncomeAndExpensesDetail { get; set; }
        public string FinancialSitutationComparison { get; set; }
        public string SurviveWithCurrentMoney { get; set; }
        public string UnexpectedExpenses { get; set; }
        public string DebtSituation { get; set; }
        public string FinalMonthlyFunds { get; set; }
        public string SemesterFinancialDecisions { get; set; }
        public string LastPurchaseDecisions { get; set; }
    }
}

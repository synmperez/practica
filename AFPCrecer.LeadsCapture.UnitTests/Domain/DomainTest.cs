﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Core;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Domain
{
    [TestClass]
    public class DomainTest
    {
        private IEncryptWizard encryptWizard;
        private IRunStoreProcedureRepository runStoreProcedureRepository;
        private IConfiguration configuration;
        [TestInitialize]
        public void TestInitialize()
        {
            var myConfiguration = new Dictionary<string, string> {
                { "DecryptOnBackendKey", "ACTR19LC$1CR3C3R" },
                { "EncryptOnBackendKey", "Eret$rtrt54$Q%trJh" },
                { "StaticURL", "https://extendsclass.com/mock/rest/aff61addd1a093b378f72c98e498a0bb" }
            };
            configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();

            encryptWizard = Substitute.For<IEncryptWizard>();
            encryptWizard.Encrypt(null, null, null).ReturnsForAnyArgs("EncryptTextOrCode");
            encryptWizard.Decrypt("Value", Arg.Any<byte[]>(), Arg.Any<byte[]>()).Returns("DencryptTextOrCode");
            encryptWizard.Decrypt("1", Arg.Any<byte[]>(), Arg.Any<byte[]>()).Returns("1");
            encryptWizard.Decrypt("2", Arg.Any<byte[]>(), Arg.Any<byte[]>()).Returns("2");
            encryptWizard.Decrypt("3", Arg.Any<byte[]>(), Arg.Any<byte[]>()).Returns("3");
            encryptWizard.Decrypt("10", Arg.Any<byte[]>(), Arg.Any<byte[]>()).Returns("10");

            runStoreProcedureRepository = Substitute.For<IRunStoreProcedureRepository>();                        
        }

        [TestMethod]
        public async Task EstimacionSuenoRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "ID", "Ahorro", "Brecha" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["ID"] = 1;
            dr["Ahorro"] = 880.49;
            dr["Brecha"] = 50.12;
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            DreamsEstimateDomain dreamsEstimateDomain = new DreamsEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await dreamsEstimateDomain.RegistryDreamsEstimateAsync(new DreamEstimate());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task EstimacionSuenoConOtherDiferenteDeNullRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "ID", "Ahorro", "Brecha" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["ID"] = 1;
            dr["Ahorro"] = 880.49;
            dr["Brecha"] = 50.12;
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            DreamsEstimateDomain dreamsEstimateDomain = new DreamsEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await dreamsEstimateDomain.RegistryDreamsEstimateAsync(new DreamEstimate() { Other = "otro"});

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimacionSuenoDevuelveNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            DreamsEstimateDomain dreamsEstimateDomain = new DreamsEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await dreamsEstimateDomain.RegistryDreamsEstimateAsync(new DreamEstimate());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimacionSuenoDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Ahorro", "Brecha" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Ahorro"] = 880.49;
            dr["Brecha"] = 50.12;
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            DreamsEstimateDomain dreamsEstimateDomain = new DreamsEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await dreamsEstimateDomain.RegistryDreamsEstimateAsync(new DreamEstimate());
        }

        [TestMethod]
        public async Task RegistrarSaludFinancieraRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "HEALTH_STATE", "HEALTH_TITLE", "RESULT_CODE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["HEALTH_STATE"] = "Estado";
            dr["HEALTH_TITLE"] = "Título";
            dr["RESULT_CODE"] = "200";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            FinancialHealthDomain financialHealthDomain = new FinancialHealthDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await financialHealthDomain.RegistryFinancialHealthStateAsync(new FinancialHealth());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarSaludFinancieraDevuelveNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            FinancialHealthDomain financialHealthDomain = new FinancialHealthDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await financialHealthDomain.RegistryFinancialHealthStateAsync(new FinancialHealth());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task RegistrarSaludFinancieraDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "HEALTH_STATE", "HEALTH_TITLE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["HEALTH_STATE"] = "Estado";
            dr["HEALTH_TITLE"] = "Título";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            FinancialHealthDomain financialHealthDomain = new FinancialHealthDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await financialHealthDomain.RegistryFinancialHealthStateAsync(new FinancialHealth());
        }

        [TestMethod]
        public async Task RegistrarInversorRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "Code", "Investor" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Code"] = "200";
            dr["Investor"] = "InvestorType";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            InvestorDomain investorDomain = new InvestorDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await investorDomain.RegistryInvestorAsync(new Investor());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarInversorNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            InvestorDomain investorDomain = new InvestorDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await investorDomain.RegistryInvestorAsync(new Investor());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarInversorDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Investor" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Investor"] = "InvestorType";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            InvestorDomain investorDomain = new InvestorDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await investorDomain.RegistryInvestorAsync(new Investor());
        }

        [TestMethod]
        public async Task RegistrarLeadRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "Code", "Nombre" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Code"] = "Value";
            dr["Nombre"] = "Value";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            LeadsDomain leadsDomain = new LeadsDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await leadsDomain.RegistryLeadAsync(new Leads());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarLeadNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            LeadsDomain leadsDomain = new LeadsDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await leadsDomain.RegistryLeadAsync(new Leads());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarLeadDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Nombre" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Nombre"] = "Value";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            LeadsDomain leadsDomain = new LeadsDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await leadsDomain.RegistryLeadAsync(new Leads());
        }

        [TestMethod]
        public async Task EstimarPensionRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "Code", "Pension" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Code"] = "200";
            dr["Pension"] = "800.60";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            PensionEstimateDomain pensionEstimateDomain = new PensionEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await pensionEstimateDomain.EstimatePensionAsync(new PensionEstimate());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimarPensionDevuelveNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            PensionEstimateDomain pensionEstimateDomain = new PensionEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await pensionEstimateDomain.EstimatePensionAsync(new PensionEstimate());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimarPensionDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Pension" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Pension"] = "800.60";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            PensionEstimateDomain pensionEstimateDomain = new PensionEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await pensionEstimateDomain.EstimatePensionAsync(new PensionEstimate());
        }

        [TestMethod]
        public async Task GetMailInfoRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "200";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dt.Rows.Add(dr);
            };

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);            

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);            
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "1", IdCampaign = "1" });

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetMailInfoNoFueExitosoCuandoRetornaCodigoDiferenteDe200()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "400";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dt.Rows.Add(dr);
            };

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "1", IdCampaign = "1" });

            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task GetMailInfoNoFueExitosoCuandoEldt2TieneMenosDe3Columnas()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["RESULT_CODE"] = "400";
            dr["EMAIL"] = "destinatario@mail.com";
            dr["NOMBRE_COMPLETO"] = "DestinatarioNombre";
            dr["SUBJECT"] = "Sunto";
            dr["MESSAGE"] = "Mensaje";
            dr["DE_EMAIL"] = "remitente@mail.com";
            dr["DE_NOMBRE"] = "RemitenteNombre";
            dt.Rows.Add(dr);

            DataTable dt2 = new DataTable("dtMock2");
            string[] column2 = { "Col1", "Col2" };
            foreach (var item in column2)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt2.Columns.Add(item);
            }

            DataRow dr2 = dt2.NewRow();
            dr2["Col1"] = "valcol1";
            dr2["Col2"] = "valcol2";            
            dt2.Rows.Add(dr2);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt2);            

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "1", IdCampaign = "1" });            
        }

        [TestMethod]
        public async Task CorreoConTipoInvestorPDFFormatExitoso()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE"};
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "200";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;                
                dt.Rows.Add(dr);
            }

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "1", IdCampaign = "1" });

            Assert.IsNotNull(result);
            Assert.AreEqual("InvestorMailFormat1 BODY", result[1].ToString());
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task CorreoConTipoFinantialHealthPDFFormatExitoso()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }
            for (int i = 0; i < 15; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "200";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dt.Rows.Add(dr);
            }

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "2", IdCampaign = "2" });

            Assert.IsNotNull(result);
            Assert.AreEqual("FinantialHealthFormat BODY", result[1].ToString());
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task GenerarCorreoConTipoDreamsEstimationPDFFormatYMetaAhorrosNoCumplidaExitoso()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE",
                "GAP_AHORROS", "AHORRO_TOTAL", "INGRESOS_MENSUALES", "INGRESOS_ADICIONALES", "META_AHORRO", "AHORROS_ACTUALES", "AHORRO_ESTIMADO", "SUENO",
                "PERIOCIDAD_INGRESOS_ADICIONALES", "PERIOCIDAD_APORTE", "ANOS_AHORRO" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "200";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dr["GAP_AHORROS"] = "1250.60";
                dr["AHORRO_TOTAL"] = "1250.60";
                dr["INGRESOS_MENSUALES"] = "500.50";
                dr["INGRESOS_ADICIONALES"] = "50.44";
                dr["META_AHORRO"] = "25000";
                dr["AHORROS_ACTUALES"] = "200";
                dr["AHORRO_ESTIMADO"] = "250";
                dr["SUENO"] = "SUEÑO";
                dr["PERIOCIDAD_INGRESOS_ADICIONALES"] = "24";
                dr["PERIOCIDAD_APORTE"] = "12";
                dr["ANOS_AHORRO"] = "10";
                dt.Rows.Add(dr);
            }

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "3", IdCampaign = "3" });

            Assert.IsNotNull(result);
            Assert.AreEqual("DreamsEstimationFormat BODY", result[1].ToString());
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task GenerarCorreoConTipoDreamsEstimationPDFFormatYMetaAhorrosCumplidaExitoso()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE",
                "GAP_AHORROS", "AHORRO_TOTAL", "INGRESOS_MENSUALES", "INGRESOS_ADICIONALES", "META_AHORRO", "AHORROS_ACTUALES", "AHORRO_ESTIMADO", "SUENO",
                "PERIOCIDAD_INGRESOS_ADICIONALES", "PERIOCIDAD_APORTE", "ANOS_AHORRO" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "200";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dr["GAP_AHORROS"] = "-1250.60";
                dr["AHORRO_TOTAL"] = "1250.60";
                dr["INGRESOS_MENSUALES"] = "500.50";
                dr["INGRESOS_ADICIONALES"] = "50.44";
                dr["META_AHORRO"] = "25000";
                dr["AHORROS_ACTUALES"] = "200";
                dr["AHORRO_ESTIMADO"] = "250";
                dr["SUENO"] = "SUEÑO";
                dr["PERIOCIDAD_INGRESOS_ADICIONALES"] = "4";
                dr["PERIOCIDAD_APORTE"] = "6";
                dr["ANOS_AHORRO"] = "10";
                dt.Rows.Add(dr);
            }

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "3", IdCampaign = "3" });

            Assert.IsNotNull(result);
            Assert.AreEqual("DreamsEstimationFormat BODY", result[1].ToString());
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task GenerarCorreoConTipoDreamsEstimationPDFFormatConPeriocidadNoCatalogadaExitoso()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE",
                "GAP_AHORROS", "AHORRO_TOTAL", "INGRESOS_MENSUALES", "INGRESOS_ADICIONALES", "META_AHORRO", "AHORROS_ACTUALES", "AHORRO_ESTIMADO", "SUENO",
                "PERIOCIDAD_INGRESOS_ADICIONALES", "PERIOCIDAD_APORTE", "ANOS_AHORRO" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }
            for (int i = 0; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "200";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dr["GAP_AHORROS"] = "-1250.60";
                dr["AHORRO_TOTAL"] = "1250.60";
                dr["INGRESOS_MENSUALES"] = "500.50";
                dr["INGRESOS_ADICIONALES"] = "50.44";
                dr["META_AHORRO"] = "25000";
                dr["AHORROS_ACTUALES"] = "200";
                dr["AHORRO_ESTIMADO"] = "250";
                dr["SUENO"] = "SUEÑO";
                dr["PERIOCIDAD_INGRESOS_ADICIONALES"] = "3";
                dr["PERIOCIDAD_APORTE"] = "1";
                dr["ANOS_AHORRO"] = "10";
                dt.Rows.Add(dr);
            }

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "3", IdCampaign = "3" });

            Assert.IsNotNull(result);
            Assert.AreEqual("DreamsEstimationFormat BODY", result[1].ToString());
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task CorreosinTipoDEFormatoRetornaCampoPdfFormatVacio()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }
            for (int i = 0; i < 15; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "200";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dt.Rows.Add(dr);
            }

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "10", IdCampaign = "10" });

            Assert.IsNotNull(result);
            Assert.AreEqual("", result[1].ToString());
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task GetMailInfoNoFueExitoso()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE",
                "GAP_AHORROS", "AHORRO_TOTAL", "INGRESOS_MENSUALES", "INGRESOS_ADICIONALES", "META_AHORRO", "AHORROS_ACTUALES", "AHORRO_ESTIMADO", "SUENO",
                "PERIOCIDAD_INGRESOS_ADICIONALES", "PERIOCIDAD_APORTE", "ANOS_AHORRO" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }
            for (int i = 0; i < 15; i++)
            {
                DataRow dr = dt.NewRow();
                dr["RESULT_CODE"] = "400";
                dr["EMAIL"] = "destinatario" + i + "@mail.com";
                dr["NOMBRE_COMPLETO"] = "DestinatarioNombre " + i;
                dr["SUBJECT"] = "Asunto " + i;
                dr["MESSAGE"] = "Mensaje " + i;
                dr["DE_EMAIL"] = "remitente" + i + "@mail.com";
                dr["DE_NOMBRE"] = "RemitenteNombre " + i;
                dr["GAP_AHORROS"] = "1250.60";
                dr["AHORRO_TOTAL"] = "1250.60";
                dr["INGRESOS_MENSUALES"] = "500.50";
                dr["INGRESOS_ADICIONALES"] = "50.44";
                dr["META_AHORRO"] = "25000";
                dr["AHORROS_ACTUALES"] = "200";
                dr["AHORRO_ESTIMADO"] = "250";
                dr["SUENO"] = "SUEÑO";
                dr["PERIOCIDAD_INGRESOS_ADICIONALES"] = "24";
                dr["PERIOCIDAD_APORTE"] = "12";
                dr["ANOS_AHORRO"] = "10";
                dt.Rows.Add(dr);
            }            

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result1 = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "1", IdCampaign = "1" });
            var result2 = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "2", IdCampaign = "2" });
            var result3 = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "3", IdCampaign = "3" });

            Assert.IsNull(result1);
            Assert.IsNull(result2);
            Assert.IsNull(result3);            
        }
    }
}
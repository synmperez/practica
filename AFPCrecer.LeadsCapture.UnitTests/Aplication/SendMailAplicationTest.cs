﻿using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Aplication
{
    [TestClass]
    public class SendMailAplicationTest
    {
        private IGeneratePDF generatePDF;
        private ISendMailFactory sendMailFactory;
        private ISendMailDomain sendMailDomain;
        [TestInitialize]
        public void TestInitialize()
        {
            sendMailDomain = Substitute.For<ISendMailDomain>();
            generatePDF = Substitute.For<IGeneratePDF>();            
            sendMailFactory = Substitute.For<ISendMailFactory>();

            string ret0 = "StringPDF";
            generatePDF.GenratePDF(null).ReturnsForAnyArgs(ret0);
                        
            sendMailFactory.sendmail(null, null).ReturnsForAnyArgs(false);
        }

        [TestMethod]
        public async Task EnviaCorreoExitoso()
        {          
            var ret = new List<string> { "destinatario@mail.com", "PDFFORMAT", "NombreDestinatario", "Asunto", "Mesaje", "remitente@mail.com", "NombreRemitente" };
            sendMailDomain.GetMailInfo(null).ReturnsForAnyArgs(ret);
            sendMailFactory.sendmail(null, null).ReturnsForAnyArgs(true);

            SendMailAplication sendMailAplication = new SendMailAplication(sendMailDomain, generatePDF, sendMailFactory);
            var result = await sendMailAplication.GetMailInfo(null);
            var val = result as Response<object>;

            Assert.AreEqual("200", val.Code);
        }

        [TestMethod]
        public async Task NoEsPosibleEnviarCorreo()
        {            
            var ret = new List<string> { "destinatario@mail.com", "PDFFORMAT", "NombreDestinatario", "Asunto", "Mesaje", "remitente@mail.com", "NombreRemitente" };
            sendMailDomain.GetMailInfo(null).ReturnsForAnyArgs(ret);
            sendMailFactory.sendmail(null, null).ReturnsForAnyArgs(false);

            SendMailAplication sendMailAplication = new SendMailAplication(sendMailDomain, generatePDF, sendMailFactory);
            var result = sendMailAplication.GetMailInfo(null);
            var val = await result as Response<object>;

            Assert.AreEqual("400", val.Code);
        }

        [TestMethod]
        public async Task NoHayResultadosDeCalculadoraParaEnviarCorreo()
        {
            List<string> ret = null;
            sendMailDomain.GetMailInfo(null).ReturnsForAnyArgs(ret);

            SendMailAplication sendMailAplication = new SendMailAplication(sendMailDomain, generatePDF, sendMailFactory);
            var result = sendMailAplication.GetMailInfo(null);
            var val = await result as Response<object>;

            Assert.AreEqual("401", val.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task EnviarCorreoRetornaErrorCuandoLaListaSoloTieneUnElemento()
        {
            ISendMailDomain sendMailDomain = Substitute.For<ISendMailDomain>();
            var ret = new List<string> { "destinatario@mail.com"};
            sendMailDomain.GetMailInfo(null).ReturnsForAnyArgs(ret);

            SendMailAplication sendMailAplication = new SendMailAplication(sendMailDomain, null, null);
            var result = await sendMailAplication.GetMailInfo(null);
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Application
{
    [TestClass]
    public class DreamsEstimateAplicationTest
    {
        [TestMethod]
        public async Task CalculoDeSuenoExitoso()
        {
            DreamEstimate dreamEstimate = new DreamEstimate()
            {
                ActualMoney = "500",
                AditionalIncome = "500",
                DreamTimeYears = "15",
                DreamType = "X",
                ExpectationSaving = "",
                IdUser = "123414",
                MontlyIncome = "152",
                Other = "",
                PeriodAdionaltIncome = "1",
                PeriodInputSaving = "4",
                SavingsGoal = "100000"
            };

            IDreamsEstimationDomain dreamsEstimationDomain = Substitute.For<IDreamsEstimationDomain>();
            var ret = "6500";
            dreamsEstimationDomain.RegistryDreamsEstimateAsync(dreamEstimate).Returns(ret);

            DreamsEstimateAplication dreamsEstimateAplication = new DreamsEstimateAplication(dreamsEstimationDomain);
            var result = await dreamsEstimateAplication.RegistryDreamsEstimateAsync(dreamEstimate);

            Assert.AreEqual("200", result.Code);
        }

        [TestMethod]
        public async Task CalculoDeSuenoRetornaNull()
        {
            DreamEstimate dreamEstimate = new DreamEstimate()
            {
                ActualMoney = "500",
                AditionalIncome = "500",
                DreamTimeYears = "15",
                DreamType = "X",
                ExpectationSaving = "",
                IdUser = "123414",
                MontlyIncome = "152",
                Other = "",
                PeriodAdionaltIncome = "1",
                PeriodInputSaving = "4",
                SavingsGoal = "100000"
            };

            IDreamsEstimationDomain dreamsEstimationDomain = Substitute.For<IDreamsEstimationDomain>();
            string ret = null;
            dreamsEstimationDomain.RegistryDreamsEstimateAsync(dreamEstimate).Returns(ret);

            DreamsEstimateAplication dreamsEstimateAplication = new DreamsEstimateAplication(dreamsEstimationDomain);
            var result = await dreamsEstimateAplication.RegistryDreamsEstimateAsync(dreamEstimate);

            Assert.IsNull(result.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task CalculoDeSuenoRetornaException()
        {
            DreamEstimate dreamEstimate = new DreamEstimate()
            {
                ActualMoney = "500",
                AditionalIncome = "500",
                DreamTimeYears = "15",
                DreamType = "X",
                ExpectationSaving = "",
                IdUser = "123414",
                MontlyIncome = "152",
                Other = "",
                PeriodAdionaltIncome = "1",
                PeriodInputSaving = "4",
                SavingsGoal = "100000"
            };

            IDreamsEstimationDomain dreamsEstimationDomain = Substitute.For<IDreamsEstimationDomain>();            
            dreamsEstimationDomain.RegistryDreamsEstimateAsync(dreamEstimate).Returns(x => { throw new Exception("Retorna excepcion para probar catch"); });

            DreamsEstimateAplication dreamsEstimateAplication = new DreamsEstimateAplication(dreamsEstimationDomain);
            var result = await dreamsEstimateAplication.RegistryDreamsEstimateAsync(dreamEstimate);            
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Aplication.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AFPCrecer.LeadsCapture.UnitTests.Aplication.DTO
{


    [TestClass]
    public class DtoTest
    {

        [TestMethod]
        public void DreamEstimateDTOPropiedadesExitoso()
        {
           
            var Obj =new DreamEstimateDTO()
             {
                 IdUser = "1",
                 DreamType = "tiposueño",
                 MontlyIncome = "500",
                 AditionalIncome = "100",
                 PeriodAdionaltIncome = "125",
                 SavingsGoal = "1000",
                 ActualMoney = "200",
                 ExpectationSaving = "300",
                 PeriodInputSaving = "2",
                 DreamTimeYears = "3",
                 Other = "otro"
             };
            Assert.IsNotNull(Obj);
            Assert.AreEqual("1", Obj.IdUser);
            Assert.AreEqual("tiposueño", Obj.DreamType);
            Assert.AreEqual("500", Obj.MontlyIncome);
            Assert.AreEqual("100", Obj.AditionalIncome);
            Assert.AreEqual("125", Obj.PeriodAdionaltIncome);
            Assert.AreEqual("1000", Obj.SavingsGoal);
            Assert.AreEqual("200", Obj.ActualMoney);
            Assert.AreEqual("300", Obj.ExpectationSaving);
            Assert.AreEqual("2", Obj.PeriodInputSaving);
            Assert.AreEqual("3", Obj.DreamTimeYears);
            Assert.AreEqual("otro", Obj.Other);

        }




        [TestMethod]
        public void FinancialHealthDTOPropiedadesExitoso()
        {
          
            var Obj = new FinancialHealthDTO() {
                IdUser = "1",
                IncomeRange = "3",
                IncomeSavedPercentageLastYear = "4%",
                IncomeAndExpensesDetail = "100",
                FinancialSitutationComparison = "20/20",
                SurviveWithCurrentMoney = "Si",
                UnexpectedExpenses = "100",
                DebtSituation = "Media",
                FinalMonthlyFunds = "200",
                SemesterFinancialDecisions = "200",
                LastPurchaseDecisions="Ok"

            };
            Assert.IsNotNull(Obj);
            Assert.AreEqual("1", Obj.IdUser);
            Assert.AreEqual("3", Obj.IncomeRange);
            Assert.AreEqual("4%", Obj.IncomeSavedPercentageLastYear);
            Assert.AreEqual("100", Obj.IncomeAndExpensesDetail);
            Assert.AreEqual("20/20", Obj.FinancialSitutationComparison);
            Assert.AreEqual("Si", Obj.SurviveWithCurrentMoney);
            Assert.AreEqual("100", Obj.UnexpectedExpenses);
            Assert.AreEqual("Media", Obj.DebtSituation);
            Assert.AreEqual("200", Obj.FinalMonthlyFunds);
            Assert.AreEqual("200", Obj.SemesterFinancialDecisions);
            Assert.AreEqual("Ok", Obj.LastPurchaseDecisions);

        }

        [TestMethod]
        public void InvestorDTOPropiedadesExitoso() {

            var Obj = new InvestorDTO() {
                IdUser = "1",
                PeriodYear = "4",
                ObjectivesConsideration = "Ok",
                InvestorLevel ="5",
                InvestorStage = "1",
                InvestorDecision = "Ok",
                InvestorPercentage = "2%",
                InvestorAge = "10",
                InvestorInvestiment = "Doc"

            };

            Assert.IsNotNull(Obj);
            Assert.AreEqual("1", Obj.IdUser);
            Assert.AreEqual("4", Obj.PeriodYear);
            Assert.AreEqual("Ok", Obj.ObjectivesConsideration);
            Assert.AreEqual("5", Obj.InvestorLevel);
            Assert.AreEqual("1", Obj.InvestorStage);
            Assert.AreEqual("Ok", Obj.InvestorDecision);
            Assert.AreEqual("2%", Obj.InvestorPercentage);
            Assert.AreEqual("10", Obj.InvestorAge);
            Assert.AreEqual("Doc", Obj.InvestorInvestiment);


        }


        [TestMethod]
        public void LeadDTOPropiedadesExitoso() {

            var Obj = new LeadDTO() {
                Id = 1,
                Name = "Ana Maria",
                Lastname = "Rivas",
                Mail = "ana@hotmail.com",
                Phone = "2222-4512",
                DUI = "024325710"

            };

            Assert.IsNotNull(Obj);
            Assert.AreEqual(1,Obj.Id);
            Assert.AreEqual("Ana Maria", Obj.Name);
            Assert.AreEqual("Rivas", Obj.Lastname);
            Assert.AreEqual("ana@hotmail.com", Obj.Mail);
            Assert.AreEqual("2222-4512", Obj.Phone);
            Assert.AreEqual("024325710", Obj.DUI);

        }



        [TestMethod]
        public void MailRequestDTOPropiedadesExitoso() {
            var Obj = new MailRequestDTO() {
                IdUser = "1",
                IdCampaign="2"

            };

            Assert.IsNotNull(Obj);
            Assert.AreEqual("1",Obj.IdUser);
            Assert.AreEqual("2",Obj.IdCampaign);
            
            ;
        }

    }
}

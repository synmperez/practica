﻿using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Domain.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Aplication
{
    [TestClass]
    public class LeadAplicationTest
    {       
        [TestMethod]
        public void RealizarRegistroLeadExitoso()
        {
            ILeadsDomain leadsDomain = Substitute.For<ILeadsDomain>();
            var ret = new string[] { "123456", "Nombre XCM" };
            leadsDomain.RegistryLeadAsync(null).ReturnsForAnyArgs(ret);

            LeadAplication financialHealthAplication = new LeadAplication(leadsDomain);
            var result = financialHealthAplication.RegistryLeadAsync(null);

            Assert.AreEqual("200", result.Result.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task RegistrarLeadLanzaException()
        {
            ILeadsDomain leadsDomain = Substitute.For<ILeadsDomain>();
            leadsDomain.RegistryLeadAsync(null).Returns(x => { throw new Exception("Retorna excepcion para probar catch"); });

            LeadAplication dreamsEstimateAplication = new LeadAplication(leadsDomain);
            var result = await dreamsEstimateAplication.RegistryLeadAsync(null);
        }
    }    
}

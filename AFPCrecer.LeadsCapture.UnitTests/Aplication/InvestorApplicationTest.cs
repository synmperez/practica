﻿using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Aplication
{
    [TestClass]
    public class InvestorApplicationTest
    {
        [TestMethod]
        public async Task SeRegistraUnInversorConExito()
        {
            Investor investor = new Investor() { IdUser = "1123456", InvestorAge = "35", InvestorDecision = "X", InvestorInvestiment = "A",
                InvestorLevel = "1", InvestorPercentage = "20", InvestorStage = "A", ObjectivesConsideration = "A", PeriodYear = "3"};

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            var ret = new string[2] { "InvestorTypeX", "200" };
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = new InvestorAplication(investorDomain);

            var result = await investorAplication.RegistryInvestorAsync(investor);
            Assert.AreEqual("200", result.Code);
        }

        [TestMethod]
        public async Task ValoresInvalidosParaRegistrar()
        {
            Investor investor = new Investor()
            {
                IdUser = "1123456",
                InvestorAge = "35",
                InvestorDecision = "X",
                InvestorInvestiment = "A",
                InvestorLevel = "1",
                InvestorPercentage = "20",
                InvestorStage = "A",
                ObjectivesConsideration = "A",
                PeriodYear = "3"
            };

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            var ret = new string[2] { null, "400" };
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = new InvestorAplication(investorDomain);

            var result = await investorAplication.RegistryInvestorAsync(investor);
            Assert.AreEqual("400", result.Code);
        }

        [TestMethod]
        public async Task NoSeEncuentraUsuarioParaRegistrar()
        {
            Investor investor = new Investor()
            {
                IdUser = "1123456",
                InvestorAge = "35",
                InvestorDecision = "X",
                InvestorInvestiment = "A",
                InvestorLevel = "1",
                InvestorPercentage = "20",
                InvestorStage = "A",
                ObjectivesConsideration = "A",
                PeriodYear = "3"
            };

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            var ret = new string[2] { null, "401" };
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = new InvestorAplication(investorDomain);

            var result = await investorAplication.RegistryInvestorAsync(investor);
            Assert.AreEqual("401", result.Code);
        }

        [TestMethod]
        public async Task RegistrarInversorRetornaUnArregloVacio()
        {
            Investor investor = new Investor()
            {
                IdUser = "1123456",
                InvestorAge = "35",
                InvestorDecision = "X",
                InvestorInvestiment = "A",
                InvestorLevel = "1",
                InvestorPercentage = "20",
                InvestorStage = "A",
                ObjectivesConsideration = "A",
                PeriodYear = "3"
            };

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            string[] ret = null;
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = new InvestorAplication(investorDomain);

            var result = await investorAplication.RegistryInvestorAsync(investor);
            Assert.AreEqual("404", result.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException), "La respuesta no tiene los datos suficientes")]
        public async Task RegistrarInversorRetornaError()
        {
            Investor investor = new Investor()
            {
                IdUser = "1123456",
                InvestorAge = "35",
                InvestorDecision = "X",
                InvestorInvestiment = "A",
                InvestorLevel = "1",
                InvestorPercentage = "20",
                InvestorStage = "A",
                ObjectivesConsideration = "A",
                PeriodYear = "3"
            };

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            var ret = new string[1] { null };
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = new InvestorAplication(investorDomain);

            var result = await investorAplication.RegistryInvestorAsync(investor);            
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Domain.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Aplication
{
    [TestClass]
    public class PensionEstimateAplicationTest
    {
        [TestMethod]
        public void EstimarPensionExitoso()
        {
            IPensionEstimateDomain pensionEstimateDomain = Substitute.For<IPensionEstimateDomain>();
            var ret = new string[] { "500.60", "200" };
            pensionEstimateDomain.EstimatePensionAsync(null).ReturnsForAnyArgs(ret);

            PensionEstimateAplication financialHealthAplication = new PensionEstimateAplication(pensionEstimateDomain);
            var result = financialHealthAplication.EstimatePensionAsync(null);

            Assert.AreEqual("200", result.Result.Code);
        }

        [TestMethod]
        public void UsuarioParaEstimarPensionNoEncontrado()
        {
            IPensionEstimateDomain pensionEstimateDomain = Substitute.For<IPensionEstimateDomain>();
            var ret = new string[] { null, "400" };
            pensionEstimateDomain.EstimatePensionAsync(null).ReturnsForAnyArgs(ret);

            PensionEstimateAplication financialHealthAplication = new PensionEstimateAplication(pensionEstimateDomain);
            var result = financialHealthAplication.EstimatePensionAsync(null);

            Assert.AreEqual("400", result.Result.Code);
        }

        [TestMethod]
        public void RegistrosParaEstimarPensionNoEncontrados()
        {
            IPensionEstimateDomain pensionEstimateDomain = Substitute.For<IPensionEstimateDomain>();
            string[] ret = null;
            pensionEstimateDomain.EstimatePensionAsync(null).ReturnsForAnyArgs(ret);

            PensionEstimateAplication financialHealthAplication = new PensionEstimateAplication(pensionEstimateDomain);
            var result = financialHealthAplication.EstimatePensionAsync(null);

            Assert.AreEqual("404", result.Result.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimarPensionRetornaError()
        {
            IPensionEstimateDomain pensionEstimateDomain = Substitute.For<IPensionEstimateDomain>();
            string[] ret = { null };
            pensionEstimateDomain.EstimatePensionAsync(null).ReturnsForAnyArgs(ret);

            PensionEstimateAplication financialHealthAplication = new PensionEstimateAplication(pensionEstimateDomain);
            var result = await financialHealthAplication.EstimatePensionAsync(null);
        }
    }
}
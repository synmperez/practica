﻿using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Domain.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Aplication
{
    [TestClass]
    public class FinancialHealthAplicationTest
    {
        [TestMethod]
        public void RegistrarSaludFinancieraExitoso()
        {
            IFinancialHealthDomain financialHealthDomain = Substitute.For<IFinancialHealthDomain>();
            var ret = new string[3] { "Titulo", "Estado", "200" };
            financialHealthDomain.RegistryFinancialHealthStateAsync(null).ReturnsForAnyArgs(ret);

            FinancialHealthAplication financialHealthAplication = new FinancialHealthAplication(financialHealthDomain);
            var result = financialHealthAplication.RegistryFinancialHealthStateAsync(null);

            Assert.AreEqual("200", result.Result.Code);
        }

        [TestMethod]
        public void UsuarioParaSaludFinancieraNoEncontrado()
        {
            IFinancialHealthDomain financialHealthDomain = Substitute.For<IFinancialHealthDomain>();
            var ret = new string[3] { null, null, "400" };
            financialHealthDomain.RegistryFinancialHealthStateAsync(null).ReturnsForAnyArgs(ret);

            FinancialHealthAplication financialHealthAplication = new FinancialHealthAplication(financialHealthDomain);
            var result = financialHealthAplication.RegistryFinancialHealthStateAsync(null);

            Assert.AreEqual("400", result.Result.Code);
        }

        [TestMethod]
        public void RegistrosParaSaludFinancieraNoEncontrados()
        {
            IFinancialHealthDomain financialHealthDomain = Substitute.For<IFinancialHealthDomain>();
            string[] ret = null;
            financialHealthDomain.RegistryFinancialHealthStateAsync(null).ReturnsForAnyArgs(ret);

            FinancialHealthAplication financialHealthAplication = new FinancialHealthAplication(financialHealthDomain);
            var result = financialHealthAplication.RegistryFinancialHealthStateAsync(null);

            Assert.AreEqual("404", result.Result.Code);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarSaludFinancieraRetornaError()
        {
            IFinancialHealthDomain financialHealthDomain = Substitute.For<IFinancialHealthDomain>();
            string[] ret = { null, "200" };
            financialHealthDomain.RegistryFinancialHealthStateAsync(null).ReturnsForAnyArgs(ret);

            FinancialHealthAplication financialHealthAplication = new FinancialHealthAplication(financialHealthDomain);
            var result = await financialHealthAplication.RegistryFinancialHealthStateAsync(null);
        }
    }
}
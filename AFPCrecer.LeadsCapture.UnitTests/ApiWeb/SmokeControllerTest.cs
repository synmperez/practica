﻿using AFPCrecer.LeadsCapture.Services.ApiWeb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AFPCrecer.LeadsCapture.UnitTests.ApiWeb
{
    [TestClass]
    public class SmokeControllerTest
    {
        [TestMethod]
        public void PruebaSmokeTest()
        {
            SmokeController smokeController = new SmokeController();
            var result = (OkObjectResult)smokeController.Test();

            Assert.IsNotNull(result);
            Assert.AreEqual("Is running...", result.Value);
        }
    }
}

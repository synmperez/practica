﻿using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Services.ApiWeb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.ApiWeb
{
    [TestClass]
    public class LeadControllerTest
    {
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException), "Las dependencias no han sido injectadas")]
        public async Task ControladorRetornaNullReferenceObjectCuandoNoSeInyectanDependencias()
        {
            LeadController leadController = new LeadController(null, null, null, null, null, null);
            Leads leads = new Leads() { Id = 1, DUI = "123064501", Lastname = "Lastname", Mail = "Mail", Name = "Name", Phone = "Phone"};
            await leadController.Post(leads);
        }

        [TestMethod]
        public void ReturnVersionService()
        {
            LeadController leadController = new LeadController(null, null, null, null, null, null);
            var result = leadController.Get();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReturnVersionServiceDebeSerString()
        {
            LeadController leadController = new LeadController(null, null, null, null, null, null);
            var result = leadController.Get();

            Assert.IsInstanceOfType(result.Value, typeof(string));
        }

        [TestMethod]
        public async Task RegistrarLeadRetornaResultadoExitoso()
        {
            Leads leads = null;

            ILeadsDomain leadsDomain = Substitute.For<ILeadsDomain>();
            var rest = new string[2] { "1", "Nombre" };
            leadsDomain.RegistryLeadAsync(leads).Returns(rest);

            LeadAplication leadAplication = Substitute.For<LeadAplication>(leadsDomain);

            LeadController leadcontroller = new LeadController(leadAplication, null, null, null, null, null);
            var response = await leadcontroller.Post(leads);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("200", value.Code);
        }

        [TestMethod]
        public async Task NoSeRegistrarLeadYRetornaNull()
        {
            Leads leads = null;

            ILeadsDomain leadsDomain = Substitute.For<ILeadsDomain>();
            string[] rest = null;
            leadsDomain.RegistryLeadAsync(leads).Returns(rest);

            LeadAplication leadAplication = Substitute.For<LeadAplication>(leadsDomain);

            LeadController leadcontroller = new LeadController(leadAplication, null, null, null, null, null);
            var response = await leadcontroller.Post(leads);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.IsNull(value.Data);
        }

        [TestMethod]
        public async Task RegistrarInversorRetornaResultadoExitoso()
        {
            Investor investor = new Investor() { IdUser = "123456", InvestorAge = "25", InvestorDecision = "Decsión", InvestorInvestiment = "Investiment",
             InvestorLevel = "Level", InvestorPercentage = "Porcentaje", InvestorStage = "Stage", ObjectivesConsideration = "Consideraciones", PeriodYear = "12"};

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            var ret = new string[2] { "InvestorTypeX", "200" };
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = Substitute.For<InvestorAplication>(investorDomain);

            LeadController leadController = new LeadController(null, investorAplication, null, null, null, null);
            var response = await leadController.Post(investor);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("200", value.Code);
        }

        [TestMethod]
        public async Task NoRegistraInversorSiLosDatosNoSonValidos()
        {
            Investor investor = null;

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            var ret = new string[2] { null, "400" };
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = Substitute.For<InvestorAplication>(investorDomain);

            LeadController leadController = new LeadController(null, investorAplication, null, null, null, null);
            var response = await leadController.Post(investor);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("400", value.Code);
        }

        [TestMethod]
        public async Task NoEncuentraUsuarioParaRegistrarInversor()
        {
            Investor investor = null;

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            var ret = new string[2] { null, null };
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = Substitute.For<InvestorAplication>(investorDomain);

            LeadController leadController = new LeadController(null, investorAplication, null, null, null, null);
            var response = await leadController.Post(investor);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("401", value.Code);
        }

        [TestMethod]
        public async Task RegistrarInversorRetorna404NotFound()
        {
            Investor investor = null;

            IInvestorDomain investorDomain = Substitute.For<IInvestorDomain>();
            string[] ret = null;
            investorDomain.RegistryInvestorAsync(investor).Returns(ret);

            InvestorAplication investorAplication = Substitute.For<InvestorAplication>(investorDomain);

            LeadController leadController = new LeadController(null, investorAplication, null, null, null, null);
            var response = await leadController.Post(investor);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("404", value.Code);
        }

        [TestMethod]
        public async Task RegistrarSaludFinancieraRetornaResultadoExitoso()
        {
            FinancialHealth financialHealth = new FinancialHealth() { AgeRange = "30", DebtSituation = "Debtsituation", Employment_Situation = "EmpSituation",
            FinalMonthlyFunds = "500.60", FinancialSitutationComparison = "Comparison", Genre = "M", IdUser = "123456", IncomeAndExpensesDetail = "IODetails",
            IncomeRange = "IRange", IncomeSavedPercentageLastYear = "35.00", LastPurchaseDecisions = "LastPdecisions", SemesterFinancialDecisions = "SFinalDecision",
            SurviveWithCurrentMoney = "1", UnexpectedExpenses = "60.65"};

            IFinancialHealthDomain financialHealthDomain = Substitute.For<IFinancialHealthDomain>();
            var ret = new string[3] { "Titulo", "Estado", "200" };
            financialHealthDomain.RegistryFinancialHealthStateAsync(financialHealth).Returns(ret);

            FinancialHealthAplication financialHealthAplication = new FinancialHealthAplication(financialHealthDomain);

            LeadController leadController = new LeadController(null, null, financialHealthAplication, null, null, null);
            var response = await leadController.Post(financialHealth);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("200", value.Code);
        }

        [TestMethod]
        public async Task NoSeEncuentraUsuarioParaRegistrarSaludFinanciera()
        {
            FinancialHealth financialHealth = null;

            IFinancialHealthDomain financialHealthDomain = Substitute.For<IFinancialHealthDomain>();
            var ret = new string[3] { null, null, "400" };
            financialHealthDomain.RegistryFinancialHealthStateAsync(null).Returns(ret);

            FinancialHealthAplication financialHealthAplication = new FinancialHealthAplication(financialHealthDomain);

            LeadController leadController = new LeadController(null, null, financialHealthAplication, null, null, null);
            var response = await leadController.Post(financialHealth);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("400", value.Code);
        }

        [TestMethod]
        public async Task NoHayDatosParaRegistrarSaludFinanciera()
        {
            FinancialHealth financialHealth = null;

            IFinancialHealthDomain financialHealthDomain = Substitute.For<IFinancialHealthDomain>();
            string[] ret = null;
            financialHealthDomain.RegistryFinancialHealthStateAsync(null).Returns(ret);

            FinancialHealthAplication financialHealthAplication = new FinancialHealthAplication(financialHealthDomain);

            LeadController leadController = new LeadController(null, null, financialHealthAplication, null, null, null);
            var response = await leadController.Post(financialHealth);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("404", value.Code);
        }

        [TestMethod]
        public async Task SeEnviaCorreoExitosamente()
        {
            MailRequest mailRequest = null;

            ISendMailDomain SendMailDomain = Substitute.For<ISendMailDomain>();
            List<string> ret = new List<string>() { "test@mail.com", "pdfFormat", "NombreDestinatario", "Asunto", "Cuerpo", "EmailRemitente", "NombreRemitente" };
            SendMailDomain.GetMailInfo(mailRequest).Returns(ret);

            IGeneratePDF generatePDF = Substitute.For<IGeneratePDF>();
            string ret0 = "StringPDF";
            generatePDF.GenratePDF(null).ReturnsForAnyArgs(ret0);

            ISendMailFactory sendMailFactory = Substitute.For<ISendMailFactory>();
            bool ret1 = true;
            sendMailFactory.sendmail(null, null).ReturnsForAnyArgs(ret1);

            SendMailAplication sendMailAplication = Substitute.For<SendMailAplication>(SendMailDomain, generatePDF, sendMailFactory);

            LeadController leadController = new LeadController(null, null, null, sendMailAplication, null, null);
            var response = await leadController.Post(mailRequest);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("200", value.Code);
        }

        [TestMethod]
        public async Task NoSePudoEnviaCorreo()
        {
            MailRequest mailRequest = null;

            ISendMailDomain SendMailDomain = Substitute.For<ISendMailDomain>();
            List<string> ret = new List<string>() { "test@mail.com", "pdfFormat", "NombreDestinatario", "Asunto", "Cuerpo", "EmailRemitente", "NombreRemitente" };
            SendMailDomain.GetMailInfo(mailRequest).Returns(ret);

            IGeneratePDF generatePDF = Substitute.For<IGeneratePDF>();
            string ret0 = "StringPDF";
            generatePDF.GenratePDF(null).ReturnsForAnyArgs(ret0);

            ISendMailFactory sendMailFactory = Substitute.For<ISendMailFactory>();
            bool ret1 = false;
            sendMailFactory.sendmail(null, null).ReturnsForAnyArgs(ret1);

            SendMailAplication sendMailAplication = Substitute.For<SendMailAplication>(SendMailDomain, generatePDF, sendMailFactory);

            LeadController leadController = new LeadController(null, null, null, sendMailAplication, null, null);
            var response = await leadController.Post(mailRequest);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("400", value.Code);
        }

        [TestMethod]
        public async Task NoSeEncuentraInformacionParaEnviarCorreo()
        {
            MailRequest mailRequest = null;

            ISendMailDomain sendMailDomain = Substitute.For<ISendMailDomain>();
            List<string> ret = null;
            sendMailDomain.GetMailInfo(null).Returns(ret);

            SendMailAplication sendMailAplication = new SendMailAplication(sendMailDomain, null, null);

            LeadController leadController = new LeadController(null, null, null, sendMailAplication, null, null);
            var response = await leadController.Post(mailRequest);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("401", value.Code);
        }

        [TestMethod]
        public async Task RealizarCalculoSuenoExitoso()
        {
            DreamEstimate DreamEstimate = null;

            IDreamsEstimationDomain dreamsEstimationDomain = Substitute.For<IDreamsEstimationDomain>();
            var rest = new string[2] { "800.50", "200" };
            dreamsEstimationDomain.RegistryDreamsEstimateAsync(DreamEstimate).Returns(rest);

            DreamsEstimateAplication dreamAplication = Substitute.For<DreamsEstimateAplication>(dreamsEstimationDomain);

            LeadController leadController = new LeadController(null, null, null, null, dreamAplication, null);
            var response = await leadController.Post(DreamEstimate);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("200", value.Code);
        }

        [TestMethod]
        public async Task RetornarNullAlRealizarCalculoYNoObtenerResultado()
        {
            DreamEstimate DreamEstimate = null;

            IDreamsEstimationDomain dreamsEstimationDomain = Substitute.For<IDreamsEstimationDomain>();
            string[] rest = null;
            dreamsEstimationDomain.RegistryDreamsEstimateAsync(DreamEstimate).Returns(rest);

            DreamsEstimateAplication dreamsEstimateAplication = Substitute.For<DreamsEstimateAplication>(dreamsEstimationDomain);

            LeadController leadController = new LeadController(null, null, null, null, dreamsEstimateAplication, null);
            var response = await leadController.Post(DreamEstimate);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual(null, value.Code);
        }

        [TestMethod]
        public async Task PensionEstimadaRetornaResultadoExitoso()
        {
            PensionEstimate pensionEstimate = null;

            IPensionEstimateDomain pensionEstimateDomain = Substitute.For<IPensionEstimateDomain>();
            var ret = new string[2] { "500.50", "200" };
            pensionEstimateDomain.EstimatePensionAsync(null).ReturnsForAnyArgs(ret);

            PensionEstimateAplication pensionEstimateAplication = Substitute.For<PensionEstimateAplication>(pensionEstimateDomain);

            LeadController leadController = new LeadController(null, null, null, null, null, pensionEstimateAplication);
            var response = await leadController.Post(pensionEstimate);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("200", value.Code);
        }

        [TestMethod]
        public async Task NoSeEncuentraUsuarioParaEstimarPension()
        {
            PensionEstimate pensionEstimate = null;

            IPensionEstimateDomain pensionEstimateDomain = Substitute.For<IPensionEstimateDomain>();
            var ret = new string[2] { null, "400" };
            pensionEstimateDomain.EstimatePensionAsync(pensionEstimate).Returns(ret);

            PensionEstimateAplication pensionEstimateAplication = Substitute.For<PensionEstimateAplication>(pensionEstimateDomain);

            LeadController leadController = new LeadController(null, null, null, null, null, pensionEstimateAplication);
            var response = await leadController.Post(pensionEstimate);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("400", value.Code);
        }

        [TestMethod]
        public async Task NoHayResultadosParaEstimarPension()
        {
            PensionEstimate pensionEstimate = null;

            IPensionEstimateDomain pensionEstimateDomain = Substitute.For<IPensionEstimateDomain>();
            string[] ret = null;
            pensionEstimateDomain.EstimatePensionAsync(pensionEstimate).Returns(ret);

            PensionEstimateAplication pensionEstimateAplication = Substitute.For<PensionEstimateAplication>(pensionEstimateDomain);

            LeadController leadController = new LeadController(null, null, null, null, null, pensionEstimateAplication);
            var response = await leadController.Post(pensionEstimate);
            var result = response.Result as OkObjectResult;
            var value = result.Value as Response<object>;

            Assert.AreEqual("404", value.Code);
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Cross.Wizard;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Cross
{
    [TestClass]
    public class GeneratePDFWizardTest
    {
        [TestMethod]
        public async Task GenerarPDFFuncionaExitosamente()
        {
            string html = "<HTML>" +
                      "<HEAD>" +
                      "<TITLE>Your Title Here</TITLE>" +
                      "</HEAD>" +
                      "<BODY BGCOLOR='FFFFFF'>" +
                      "<CENTER>IMAGEN</CENTER>" +
                      "<HR>" +
                      "<a href='http://somegreatsite.com'>Link Name</a>" +
                      "is a link to another nifty site" +
                      "<H1>This is a Header</H1>" +
                      "<H2>This is a Medium Header</H2>" +
                      "Send me mail at <a href='mailto:support@yourcompany.com'>" +
                      "support@yourcompany.com</a>." +
                      "<P> This is a new paragraph!" +
                      "<P> <B>This is a new paragraph!</B>" +
                      "<BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B>" +
                      "<HR>" +
                      "</BODY>" +
                      "</HTML>";

            GeneratePDFWizard generatePDFWizard = new GeneratePDFWizard();
            var result = await generatePDFWizard.GenratePDF(html);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GenerarPDF2FuncionaExitosamente()
        {
            string html = "<HTML>" +
                      "<HEAD>" +
                      "<TITLE>Your Title Here</TITLE>" +
                      "</HEAD>" +
                      "<BODY BGCOLOR='FFFFFF'>" +
                      "<CENTER>IMAGEN</CENTER>" +
                      "<HR>" +
                      "<a href='http://somegreatsite.com'>Link Name</a>" +
                      "is a link to another nifty site" +
                      "<H1>This is a Header</H1>" +
                      "<H2>This is a Medium Header</H2>" +
                      "Send me mail at <a href='mailto:support@yourcompany.com'>" +
                      "support@yourcompany.com</a>." +
                      "<P> This is a new paragraph!" +
                      "<P> <B>This is a new paragraph!</B>" +
                      "<BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B>" +
                      "<HR>" +
                      "</BODY>" +
                      "</HTML>";

            var result = GeneratePDFWizard.GenratePDF2(html);                        
            Assert.IsNotNull(result);
        }
    }
}
﻿using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AFPCrecer.LeadsCapture.Cross.Wizard;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;

namespace AFPCrecer.LeadsCapture.UnitTest.Cross
{
    [TestClass]
    public class EncryptWizardTest
    {
        private IConfiguration configuration;
        [TestInitialize]
        public void TestInitialize()
        {
            var myConfiguration = new Dictionary<string, string> {
                { "DecryptOnBackendKey", "ACTR19LC$1CR3C3R" },
                { "EncryptOnBackendKey", "Eret$rtrt54$Q%trJh" },
            };
            configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();
        }

        [TestMethod]
        public void EncriptarConEncryptOnBackendKeyRealizadoConExito()
        {
            EncryptWizard encryptWizard = new EncryptWizard();

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(configuration["EncryptOnBackendKey"]));
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            Assert.IsNotNull(encryptWizard.Encrypt("Texto", key, iv));
        }

        [TestMethod]
        public void DesencriptarConEncryptOnBackendKeyRealizadoConExito()
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(configuration["EncryptOnBackendKey"]));
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };            

            EncryptWizard encryptWizard = new EncryptWizard();
            var result = encryptWizard.Decrypt("sjGabtysnT0ZOE/aE1Mcvg==", key, iv);
            
            Assert.IsNotNull(result);
            Assert.AreEqual("Texto", result);
        }

        [TestMethod]
        [ExpectedException(typeof(CryptographicException), "Llave de encriptacion invalida ")]
        public void FallaDesencriptarConLlaveEquivocadaEncryptOnBackendKey()
        {
            EncryptWizard encryptWizard = new EncryptWizard();

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(configuration["DecryptOnBackendKey"]));
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
            var result = encryptWizard.Decrypt("sjGabtysnT0ZOE/aE1Mcvg==", key, iv);
            Assert.IsNotNull(result);
            Assert.AreEqual("Texto", result);
        }

        [TestMethod]
        public void EncriptarConDecryptOnBackendKeyRealizadoConExito()
        {
            EncryptWizard encryptWizard = new EncryptWizard();

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(configuration["DecryptOnBackendKey"]));
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            Assert.IsNotNull(encryptWizard.Encrypt("Texto", key, iv));
        }

        [TestMethod]
        public void DesencriptarConDecryptOnBackendKeyRealizadoConExito()
        {
            EncryptWizard encryptWizard = new EncryptWizard();

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(configuration["DecryptOnBackendKey"]));
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
            var result = encryptWizard.Decrypt("0xCmnaf+SwbyqufLaCkakw==", key, iv);
            Assert.IsNotNull(result);
            Assert.AreEqual("Texto", result);
        }

        [TestMethod]
        [ExpectedException(typeof(CryptographicException), "Llave de encriptacion invalida ")]
        public void FallaDesencriptarConLlaveEquivocadaDecryptOnBackendKey()
        {
            EncryptWizard encryptWizard = new EncryptWizard();

            SHA256 mySHA256 = SHA256Managed.Create();
            byte[] key = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(configuration["EncryptOnBackendKey"]));
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
            var result = encryptWizard.Decrypt("0xCmnaf+SwbyqufLaCkakw==", key, iv);
            Assert.IsNotNull(result);
            Assert.AreEqual("Texto", result);
        }
    }
}
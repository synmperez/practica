﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using AFPCrecer.LeadsCapture.Cross.Wizard;

namespace AFPCrecer.LeadsCapture.UnitTest.Cross
{
    [TestClass]
    public class WebManagerTest
    {
        private IConfiguration configuration;
        [TestInitialize]
        public void TestInitialize()
        {
            var myConfiguration = new Dictionary<string, string> {
                { "SendGrid:Endpoint", "https://api.testservernotfound.com/v3/mail/send" },
                { "SendGrid:ApiKey", "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" },
            };
            configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();
        }

        [TestMethod]
        [ExpectedException(typeof(System.Net.WebException))]
        public void ElServidorRespondeNotFound400()
        {
            string endpoint = configuration["SendGrid:Endpoint"];
            string apikey = configuration["SendGrid:ApiKey"];
            string bearer = String.Format("Authorization: Bearer {0}", apikey);

            List<string> headers = new List<string>() { bearer };
            var result = WebManager.PerformRequest("JSONDATA", headers, endpoint, "application/json");
        }

        [TestMethod]
        public void ElServidorRespondeMethodPost()
        {
            string endpoint = "https://httpbin.org/post"; //Api publica de Prueba
            string apikey = configuration["SendGrid:ApiKey"];
            string bearer = String.Format("Authorization: Bearer {0}", apikey);
            List<string> headers = new List<string>() { bearer };
            var result = WebManager.PerformRequest("JSONDATA", headers, endpoint, "application/json");
            Assert.AreEqual(200, result.Item1);
            Assert.IsNotNull(result.Item2);
        }

        [TestMethod]
        public void ElServidorRespondeGet()
        {
            string endpoint = "https://httpbin.org/get"; //Api publica de Prueba
            string apikey = configuration["SendGrid:ApiKey"];
            string bearer = String.Format("Authorization: Bearer {0}", apikey);
            List<string> headers = new List<string>() { bearer };
            var result = WebManager.PerformRequest("JSONDATA", headers, endpoint, "application/json", "GET");
            Assert.AreEqual(0, result.Item1);
            Assert.IsNotNull(result.Item2);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using AFPCrecer.LeadsCapture.Infrastructure.Data;
using System.Data.SqlClient;

namespace AFPCrecer.LeadsCapture.UnitTests.Infrastructure.Data
{
    [TestClass]
    public class ConnectionFactoryTest
    {

        private IConfiguration configuration;

        [TestInitialize]
        public void TestInitialize()
        {

            var myConfiguration = new Dictionary<string, string> {
                { "DecryptOnBackendKey", "ACTR19LC$1CR3C3R" },
                { "EncryptOnBackendKey", "Eret$rtrt54$Q%trJh" },
                { "StaticURL", "" },
                { "connectionString", "Server=tcp:crecerdata.database.windows.net,1433;Initial Catalog=afpcrecer;Persist Security Info=False;User ID=consulting;Password=Treming2019;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;" }
            };
            configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();
        }


        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void GetConnectionValidastatusdeConeccionFail()
        {

            ConnectionFactory connectionactory = new ConnectionFactory(configuration);
            var sqlConnection = new SqlConnection();
            sqlConnection = connectionactory.GetConnection;
            Assert.IsNotNull(sqlConnection);
            Assert.AreEqual(System.Data.ConnectionState.Open, sqlConnection.State);


        }



        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void GetConnectionFailServerNoValido()
        {            
            ConnectionFactory connectionactory = new ConnectionFactory(configuration);
            var sqlConnection = new SqlConnection();
            sqlConnection = connectionactory.GetConnection;
            
        }

    }
}

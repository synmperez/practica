﻿using AFPCrecer.LeadsCapture.Cross.Wizard;
using AFPCrecer.LeadsCapture.Infrastructure.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Infrastructure.Data
{
    [TestClass]
    public class SendMailFactoryTest
    {
        private IConfiguration configuration;
        private IConfiguration configurationFail;
        [TestInitialize]
        public void TestInitialize()
        {
            var myConfiguration = new Dictionary<string, string> {
                { "SendGrid:Endpoint", "https://extendsclass.com/mock/rest/e7a760919d685001e3eb869dc4efd7a3/sendMailfake" },
                { "SendGrid:ApiKey", "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" },
            };
            configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();

            var myConfigurationFail = new Dictionary<string, string> {
                { "SendGrid:Endpoint", "https://extendsclass.com/mock/rest/e7a760919d685001e3eb869dc4efd7a3/sendMailfake2" },
                { "SendGrid:ApiKey", "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" },
            };
            configurationFail = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfigurationFail)
                .Build();
        }

        [TestMethod]
        public async Task SeEnvianCorreosCorrectamente()
        {
            var lst = new List<string> { "destinatario@mail.com", "PDFFORMAT", "NombreDestinatario", "Asunto", "Mesaje", "remitente@mail.com", "NombreRemitente" };
            SendMailFactory sendMailFactory = new SendMailFactory(configuration);            
            var result = await sendMailFactory.sendmail(lst, "File");
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public async Task ElServidorDevuelveCodigoDiferenteDe202YRetornaFalso()
        {
            var lst = new List<string> { "destinatario@mail.com", "PDFFORMAT", "NombreDestinatario", "Asunto", "Mesaje", "remitente@mail.com", "NombreRemitente" };
            SendMailFactory sendMailFactory = new SendMailFactory(configurationFail);
            var result = await sendMailFactory.sendmail(lst, "File");
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public async Task SeProduceUnError404YEsCapturadoRetornaFalso()
        {            
            var lst = new List<string> { "destinatario@mail.com", "PDFFORMAT", "NombreDestinatario", "Asunto", "Mesaje", "remitente@mail.com", "NombreRemitente" };
            SendMailFactory sendMailFactory = new SendMailFactory(configurationFail);
            var result = await sendMailFactory.sendmail(lst, "File");
            Assert.AreEqual(false, result);
        }
    }
}

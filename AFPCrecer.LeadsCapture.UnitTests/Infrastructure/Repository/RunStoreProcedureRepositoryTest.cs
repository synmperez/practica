﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Infrastructure.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.Infrastructure.Repository
{
  [TestClass]
   public class RunStoreProcedureRepositoryTest
    {
        private IConfiguration configuration;

        [TestInitialize]
        public void TestInitialize()
        {

            var myConfiguration = new Dictionary<string, string> {
                { "DecryptOnBackendKey", "ACTR19LC$1CR3C3R" },
                { "EncryptOnBackendKey", "Eret$rtrt54$Q%trJh" },
                { "StaticURL", "" },
                { "connectionString", "Server=tcp:crecerdata.database.windows.net,1433;Initial Catalog=afpcrecer;Persist Security Info=False;User ID=consulting;Password=Treming2019;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;" }
            };
            configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public async Task RunStoreProcedureSQLServerAsyncNoConecta() {
            IConnectionFactory conectionFactory = Substitute.For<IConnectionFactory>();
            RunStoreProcedureRepository runStoreProcedureRepository = new RunStoreProcedureRepository(conectionFactory);
            await runStoreProcedureRepository.RunStoreProcedureSQLServerAsync("Sp",new string[,] { { "@LEAD_USER", ""},{ "@SURVEY","" } });


        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]

        public  async Task RunStoreProcedureSQLServerAsynRetornFallido()
        {
            IConnectionFactory ConnectionFactory = Substitute.For<IConnectionFactory>();
            ConnectionFactory.GetConnection.Returns(new System.Data.SqlClient.SqlConnection());
            RunStoreProcedureRepository runStoreprocedurerepository = new RunStoreProcedureRepository(ConnectionFactory);
            await runStoreprocedurerepository.RunStoreProcedureSQLServerAsync("sp",new string[,] { { "@LEAD_USER", "" }, { "@SURVEY", "" } });



        }


    }
}

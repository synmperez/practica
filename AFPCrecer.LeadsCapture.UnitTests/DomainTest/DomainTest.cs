﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Core;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.UnitTests.DomainTest
{
    [TestClass]
    public class DomainTest
    {
        private IEncryptWizard encryptWizard;
        private IRunStoreProcedureRepository runStoreProcedureRepository;
        private IConfiguration configuration;
        [TestInitialize]
        public void TestInitialize()
        {
            encryptWizard = Substitute.For<IEncryptWizard>();
            encryptWizard.Encrypt(null, null, null).ReturnsForAnyArgs("EncryptTextOrCode");
            encryptWizard.Decrypt("Value", null, null).ReturnsForAnyArgs("DencryptTextOrCode");
            encryptWizard.Decrypt("1", null, null).ReturnsForAnyArgs("10005");
            runStoreProcedureRepository = Substitute.For<IRunStoreProcedureRepository>();
            var myConfiguration = new Dictionary<string, string> {
                { "DecryptOnBackendKey", "ACTR19LC$1CR3C3R" },
                { "EncryptOnBackendKey", "Eret$rtrt54$Q%trJh" },
                { "StaticURL", "" }
            };
            configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();
        }

        [TestMethod]
        public async Task EstimacionSuenoRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "ID", "Ahorro", "Brecha" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["ID"] = 1;
            dr["Ahorro"] = 880.49;
            dr["Brecha"] = 50.12;
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            DreamsEstimateDomain dreamsEstimateDomain = new DreamsEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await dreamsEstimateDomain.RegistryDreamsEstimateAsync(new DreamEstimate());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimacionSuenoDevuelveNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            DreamsEstimateDomain dreamsEstimateDomain = new DreamsEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await dreamsEstimateDomain.RegistryDreamsEstimateAsync(new DreamEstimate());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimacionSuenoDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Ahorro", "Brecha" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Ahorro"] = 880.49;
            dr["Brecha"] = 50.12;
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            DreamsEstimateDomain dreamsEstimateDomain = new DreamsEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await dreamsEstimateDomain.RegistryDreamsEstimateAsync(new DreamEstimate());
        }

        [TestMethod]
        public async Task RegistrarSaludFinancieraRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "HEALTH_STATE", "HEALTH_TITLE", "RESULT_CODE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["HEALTH_STATE"] = "Estado";
            dr["HEALTH_TITLE"] = "Título";
            dr["RESULT_CODE"] = "200";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            FinancialHealthDomain financialHealthDomain = new FinancialHealthDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await financialHealthDomain.RegistryFinancialHealthStateAsync(new FinancialHealth());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarSaludFinancieraDevuelveNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            FinancialHealthDomain financialHealthDomain = new FinancialHealthDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await financialHealthDomain.RegistryFinancialHealthStateAsync(new FinancialHealth());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task RegistrarSaludFinancieraDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "HEALTH_STATE", "HEALTH_TITLE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["HEALTH_STATE"] = "Estado";
            dr["HEALTH_TITLE"] = "Título";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            FinancialHealthDomain financialHealthDomain = new FinancialHealthDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await financialHealthDomain.RegistryFinancialHealthStateAsync(new FinancialHealth());
        }

        [TestMethod]
        public async Task RegistrarInversorRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "Code", "Investor" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Code"] = "200";
            dr["Investor"] = "InvestorType";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            InvestorDomain investorDomain = new InvestorDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await investorDomain.RegistryInvestorAsync(new Investor());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarInversorNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            InvestorDomain investorDomain = new InvestorDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await investorDomain.RegistryInvestorAsync(new Investor());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarInversorDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Investor" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Investor"] = "InvestorType";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            InvestorDomain investorDomain = new InvestorDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await investorDomain.RegistryInvestorAsync(new Investor());
        }

        [TestMethod]
        public async Task RegistrarLeadRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "Code", "Nombre" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Code"] = "Value";
            dr["Nombre"] = "Value";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            LeadsDomain leadsDomain = new LeadsDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await leadsDomain.RegistryLeadAsync(new Leads());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarLeadNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            LeadsDomain leadsDomain = new LeadsDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await leadsDomain.RegistryLeadAsync(new Leads());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task RegistrarLeadDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Nombre" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Nombre"] = "Value";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            LeadsDomain leadsDomain = new LeadsDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await leadsDomain.RegistryLeadAsync(new Leads());
        }

        [TestMethod]
        public async Task EstimarPensionRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "Code", "Pension" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Code"] = "200";
            dr["Pension"] = "800.60";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            PensionEstimateDomain pensionEstimateDomain = new PensionEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await pensionEstimateDomain.EstimatePensionAsync(new PensionEstimate());

            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimarPensionDevuelveNullReferenceCuandoElDataTableNoContieneFilas()
        {
            DataTable dt = new DataTable();
            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            PensionEstimateDomain pensionEstimateDomain = new PensionEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await pensionEstimateDomain.EstimatePensionAsync(new PensionEstimate());
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public async Task EstimarPensionDevuelveNullReferenceCuandoElDataTableNoContieneTodasLasColumnas()
        {
            DataTable dt = new DataTable();
            string[] column = { "Pension" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["Pension"] = "800.60";
            dt.Rows.Add(dr);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            PensionEstimateDomain pensionEstimateDomain = new PensionEstimateDomain(runStoreProcedureRepository, configuration, encryptWizard);
            var result = await pensionEstimateDomain.EstimatePensionAsync(new PensionEstimate());
        }

        [TestMethod]
        public async Task GetMailInfoRealizadaConExito()
        {
            DataTable dt = new DataTable("dtMock");
            string[] column = { "RESULT_CODE", "EMAIL", "NOMBRE_COMPLETO", "SUBJECT", "MESSAGE", "DE_EMAIL", "DE_NOMBRE" };
            foreach (var item in column)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt.Columns.Add(item);
            }

            DataRow dr = dt.NewRow();
            dr["RESULT_CODE"] = "200";
            dr["EMAIL"] = "destinatario@mail.com";
            dr["NOMBRE_COMPLETO"] = "DestinatarioNombre";
            dr["SUBJECT"] = "Sunto";
            dr["MESSAGE"] = "Mensaje";
            dr["DE_EMAIL"] = "remitente@mail.com";
            dr["DE_NOMBRE"] = "RemitenteNombre";
            dt.Rows.Add(dr);

            DataTable dt2 = new DataTable("dtMock2");
            string[] column2 = { "Col1", "Col2", "Col3" };
            foreach (var item in column2)
            {
                DataColumn dtcolumn = new DataColumn(item);
                dtcolumn.AllowDBNull = true;
                dt2.Columns.Add(item);
            }

            DataRow dr2 = dt2.NewRow();
            dr2["Col1"] = "valcol1";
            dr2["Col2"] = "valcol2";
            dr2["Col3"] = "valcol3";
            dt2.Rows.Add(dr2);

            runStoreProcedureRepository.RunStoreProcedureSQLServerAsync(null, null).ReturnsForAnyArgs(dt);

            SendMailDomain sendMailDomain = new SendMailDomain(configuration, runStoreProcedureRepository, encryptWizard);
            var result = await sendMailDomain.GetMailInfo(new MailRequest() { IdUser = "1", IdCampaign = "1" });

            Assert.IsNotNull(result);
        }
    }
}

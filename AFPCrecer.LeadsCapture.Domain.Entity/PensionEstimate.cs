﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFPCrecer.LeadsCapture.Domain.Entity
{
    public class PensionEstimate
    {
        public string IdUser { get; set; }
        public string Genre { get; set; }
        public string SppTime { get; set; }
        public string SppSalary { get; set; }
        public string SppActualBalance { get; set; }
        public string BirthDate { get; set; }
        public string QuotedActualTimeSap { get; set; }
        public string ActualSalary { get; set; }
        public string RetirementDesiredAge { get; set; }
    }
}

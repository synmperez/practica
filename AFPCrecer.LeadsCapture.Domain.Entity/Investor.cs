﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFPCrecer.LeadsCapture.Domain.Entity
{
   public class Investor
   {
        public string IdUser { get; set; }
        public string PeriodYear { get; set; } 
        public string ObjectivesConsideration { get; set; }
        public string InvestorLevel { get; set; }
        public string InvestorStage { get; set; }
        public string InvestorDecision { get; set; }
        public string InvestorPercentage { get; set; }
        public string InvestorAge { get; set; }
        public string InvestorInvestiment { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFPCrecer.LeadsCapture.Domain.Entity
{
    public class DreamEstimate
    {
        public string IdUser { get; set; }
        public string DreamType { get; set; }
        public string MontlyIncome { get; set; } 
        public string AditionalIncome { get; set; }
  
        public string PeriodAdionaltIncome { get; set; }
     
        public string SavingsGoal { get; set; }
       
        public string ActualMoney { get; set; }

        public string ExpectationSaving { get; set; }
   
        public string PeriodInputSaving { get; set; }

        public string DreamTimeYears { get; set; }

        public string Other { get; set; }
    }
}

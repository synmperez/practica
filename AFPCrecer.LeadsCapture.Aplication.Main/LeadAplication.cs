﻿using System;
using AutoMapper;
using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Cross.Common;
using System.Threading.Tasks;
using System.Collections.Generic;
namespace AFPCrecer.LeadsCapture.Aplication.Main
{
    public class LeadAplication : ILeadAplication
    {
        private readonly ILeadsDomain _leadsDomain;

        public LeadAplication(ILeadsDomain leadsDomain)
        {
            _leadsDomain = leadsDomain;
        }

        public async Task<Response<object>> RegistryLeadAsync(Leads lead)
        {
            var response = new Response<object>();
            try
            {
                //var lead = _mapper.Map<Leads>(leadDTO);
                response.Data =await _leadsDomain.RegistryLeadAsync(lead);
                if(response.Data!=null)
               {
                    response.Code = "200";
                    response.Description = "Registro exitoso";
                    
                }
            }catch(Exception ex)
            {
                response.Code = "500";
                response.Description = ex.Message;
                throw;
                
            }
            return response;
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Main
{
    public class SendMailAplication: ISendMailAplication
    {
        private readonly ISendMailDomain _sendMailDomain;
        private readonly IGeneratePDF _generatePDF;
        private readonly ISendMailFactory _sendMailFactory;
        public SendMailAplication(ISendMailDomain sendMailDomain, IGeneratePDF generatePDF,ISendMailFactory sendMailFactory)
        {
            _sendMailDomain = sendMailDomain;
            _generatePDF = generatePDF;
            _sendMailFactory = sendMailFactory;
        }
        public async Task<object> GetMailInfo(MailRequest mailInfo)
        {
            var response = new Response<object>();
            try
            {
                //var mailRequest = _mapper.Map<MailRequest>(mailInfo);
                List<string> data = await _sendMailDomain.GetMailInfo(mailInfo);
                if (data != null)
                {
                    if (!String.IsNullOrEmpty(data[1]))
                    {
                        string PDF = await _generatePDF.GenratePDF(data[1]);
                        bool respon = await _sendMailFactory.sendmail(data, PDF);
                        if (respon)
                        {
                            response.Code = "200";
                            response.Description = "El correo ha sido enviado con éxito";
                        }
                        else
                        {
                            response.Code = "400";
                            response.Description = "No fue posible enviar el correo";
                        }
                    }
                }
                /*Si el usuario no tiene ningún registro asociado no se puede enviar el correo, 
                 * aunque el procedimiento*/
                else
                {
                    response.Code = "401";
                    response.Description = "El usuario no tiene ningún resultado asociado para esa calculadora";
                }
            }
            catch (Exception ex)
            {
                response.Code = "500";
                response.Description = ex.Message;
                throw;
            }
            return response;
        }
    }
}

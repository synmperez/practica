﻿using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Main
{
    public class InvestorAplication:IInvestorAplication
    {
        private readonly IInvestorDomain _investorDomain;

        public InvestorAplication(IInvestorDomain investorDomain)
        {
            _investorDomain = investorDomain;
        }

        public async Task<Response<object>> RegistryInvestorAsync(Investor investor)
        {
            var response = new Response<object>();
            string InvestorType;
            try
            {
               // var investor = _mapper.Map<Investor>(investorDTO);
                string[] data= await _investorDomain.RegistryInvestorAsync(investor);
                //response.Data
                if (data != null)
                {
                    if (data[1] == "200")
                    {
                        InvestorType = data[0];
                        response.Data = new { InvestorType };
                        response.Code = "200";
                        response.Description = "Registro exitoso";
                    }
                    else if(data[1] == "400")
                    {
                        InvestorType = null;
                        response.Data = new { InvestorType };
                        response.Code = "400";
                        response.Description = "Los valores introducidos no son correctos";
                    }
                    else
                    {
                        InvestorType = null;
                        response.Data = new { InvestorType };
                        response.Code = "401";
                        response.Description = "Usuario no encontrado";
                    }
                }
                else
                {
                    InvestorType = null;
                    response.Data = new { InvestorType };
                    response.Code = "404";
                    response.Description = "Registros no encontrados";
                }
            }catch(Exception ex)
            {
                response.Code = "500";
                response.Description = ex.Message;
                throw;
            }
            return response;
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AutoMapper;
using System;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Main
{
    public class FinancialHealthAplication: IFinancialHealthAplication
    {
        private readonly IFinancialHealthDomain _financialHealth;
        private readonly IMapper _mapper;

        public FinancialHealthAplication(IFinancialHealthDomain financialHealth)
        {
            _financialHealth = financialHealth;
        }

        public async Task<Response<object>> RegistryFinancialHealthStateAsync(FinancialHealth financialHealth)
        {
            var response = new Response<object>();
            string HealthTitle;
            string HealthState;
            try
            {
                // var investor = _mapper.Map<FinancialHealth>(financialHealthDTO);
                string[] data = await _financialHealth.RegistryFinancialHealthStateAsync(financialHealth);
                if (data!= null)
                {
                    if(data[2]=="200")
                    {
                        HealthTitle = data[0];
                        HealthState = data[1];
                        response.Code = "200";
                        response.Description = "Calculos exitosos";
                        response.Data = new { HealthTitle, HealthState };
                    }
                    if (data[2] == "400")
                    {
                        HealthTitle =null;
                        HealthState = null;
                        response.Code = "400";
                        response.Description = "Usuario no encontrado";
                        response.Data = new { HealthTitle, HealthState };
                    }

                }
                else
                {
                    response.Code = "404";
                    response.Description = "Registros no encontrados";
                }
            }
            catch (Exception ex)
            {
                response.Code = "500";
                response.Description = ex.Message;
                throw;
            }
            return response;
        }
    }
}

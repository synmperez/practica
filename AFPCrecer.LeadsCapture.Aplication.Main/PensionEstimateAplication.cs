﻿using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Main
{
    public class PensionEstimateAplication: IPensionEstimateAplication
    {
        private readonly IPensionEstimateDomain _pensionEstimateDomain;

        public PensionEstimateAplication(IPensionEstimateDomain pensionEstimateDomain)
        {
            _pensionEstimateDomain = pensionEstimateDomain;
        }

        public async Task<Response<object>> EstimatePensionAsync(PensionEstimate pensionEstimate)
        {
            var response = new Response<object>();
            string Pension;
            try
            {
                string[] data = await _pensionEstimateDomain.EstimatePensionAsync(pensionEstimate);
                if (data != null)
                {
                    if (data[1] == "200")
                    {
                        Pension = data[0];
                        response.Code = "200";
                        response.Description = "Calculos exitosos";
                        response.Data = new { Pension };
                    }
                    if (data[1] == "400")
                    {
                        Pension = null;
                        response.Code = "400";
                        response.Description = "Usuario no encontrado";
                        response.Data = new { Pension };
                    }

                }
                else
                {
                    response.Code = "404";
                    response.Description = "Registros no encontrados";
                }
            }
            catch (Exception ex)
            {
                response.Code = "500";
                response.Description = ex.Message;
                throw;
            }
            return response;
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Main
{
   public class DreamsEstimateAplication: IDreamsEstimateAplication
    {
        private readonly IDreamsEstimationDomain _dreamsEstimationDomain;

        public DreamsEstimateAplication(IDreamsEstimationDomain dreamsEstimationDomain)
        {
            _dreamsEstimationDomain = dreamsEstimationDomain;
        }

        public async Task<Response<object>> RegistryDreamsEstimateAsync(DreamEstimate dreamsEstimate)
        {
            var response = new Response<object>();
            try
            {
                //var dreams = _mapper.Map<DreamEstimate>(dreamsEstimateDTO);
               response.Data = await _dreamsEstimationDomain.RegistryDreamsEstimateAsync(dreamsEstimate);
                if (response.Data != null)
                {
                    response.Code = "200";
                    response.Description = "Calculo exitoso";
                }
            }
            catch (Exception ex)
            {
                response.Code = "500";
                response.Description = ex.Message;
                throw;
            }
            return response;
        }
    }
}

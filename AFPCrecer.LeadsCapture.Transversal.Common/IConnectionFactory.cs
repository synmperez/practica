﻿using System.Data;

namespace AFPCrecer.LeadsCapture.Transversal.Common
{
    public interface IConnectionFactory
    {
        IDbConnection GetConnection { get; }
    }
}

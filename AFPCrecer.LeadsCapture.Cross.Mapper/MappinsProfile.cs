﻿using AutoMapper;
using System;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Aplication.DTO;

namespace AFPCrecer.LeadsCapture.Cross.Mapper
{
    public class MappinsProfile:Profile
    {
        public MappinsProfile()
        {
            CreateMap<Leads, LeadDTO>().ReverseMap();
            CreateMap<Investor, InvestorDTO>().ReverseMap();
            CreateMap<FinancialHealth, FinancialHealthDTO>().ReverseMap();
            CreateMap<MailRequest, MailRequestDTO>().ReverseMap();
            CreateMap<DreamEstimate, DreamEstimateDTO>().ReverseMap();
        }
    }
}

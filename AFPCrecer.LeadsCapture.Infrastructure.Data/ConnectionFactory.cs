﻿using System;
using AFPCrecer.LeadsCapture.Cross.Common;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace AFPCrecer.LeadsCapture.Infrastructure.Data
{
    public class ConnectionFactory:IConnectionFactory
    {
        private readonly IConfiguration _configuration;
        public ConnectionFactory(IConfiguration configuration)
        {
            _configuration = configuration;

        }

        public SqlConnection GetConnection
        {
            get
            {
                var sqlConnection = new SqlConnection();
                if(sqlConnection ==null) return null;

                sqlConnection.ConnectionString = _configuration["connectionString"];
                sqlConnection.Open();
                return sqlConnection;
            }
        }
    }
}

﻿using MimeKit;
using MailKit.Net.Smtp;
using AFPCrecer.LeadsCapture.Cross.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Microsoft.Extensions.Configuration;
using AFPCrecer.LeadsCapture.Cross;
using AFPCrecer.LeadsCapture.Cross.Wizard;

namespace AFPCrecer.LeadsCapture.Infrastructure.Data
{
    public class SendMailFactory : ISendMailFactory
    {

        private readonly IConfiguration _configuration;
        public SendMailFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<bool> sendmail( List<string> email, string file)
        {
            bool sended = false;
            try
            {
                string endpoint = _configuration["SendGrid:Endpoint"];
                string apikey = _configuration["SendGrid:ApiKey"];
                
                string bearer = String.Format("Authorization: Bearer {0}", apikey);
                List<string> headers = new List<string>() { bearer };

                var json = new
                {
                    personalizations = new List<object>()
                    {
                        new { to = new List<object>() { new { email = email[0], name = email[2] } }, subject = email[3] }
                    },
                    content = new List<object>()
                    {
                        new { type = "text/html", value = email[4] }
                    },
                    attachments = new List<object>()
                    {
                        new { content = file, type = "application/pdf", filename = "Resultados.pdf"}
                    },
                    from = new
                    {
                        email = email[5],
                        name = email[6]
                    },
                    reply_to = new
                    {
                        email = email[5],
                        name = email[6]
                    }
                };
                Tuple<int, string> holder = WebManager.PerformRequest(json, headers, endpoint, "application/json");
                if (holder.Item1 == 202)
                {
                    sended = true;
                }
            } catch (Exception err)
            {
                sended = false;
            }

            return sended;
        }
    }
}

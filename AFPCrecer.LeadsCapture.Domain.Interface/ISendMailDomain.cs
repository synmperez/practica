﻿using AFPCrecer.LeadsCapture.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Domain.Interface
{
    public interface ISendMailDomain
    {
        Task<List<string>> GetMailInfo(MailRequest mail);
    }
}

﻿using System.Data;
using System.Threading.Tasks;
using AFPCrecer.LeadsCapture.Domain.Entity;

namespace AFPCrecer.LeadsCapture.Domain.Interface
{
    public interface IInvestorDomain
    {
        Task<string[]> RegistryInvestorAsync(Investor investor);
    }
}

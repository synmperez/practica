﻿using AFPCrecer.LeadsCapture.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Domain.Interface
{
  public interface IPensionEstimateDomain
    {
        Task<string[]> EstimatePensionAsync(PensionEstimate pensionEstimate);
    }
}

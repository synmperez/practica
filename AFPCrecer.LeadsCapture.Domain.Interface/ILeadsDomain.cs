﻿using System.Threading.Tasks;
using AFPCrecer.LeadsCapture.Domain.Entity;

namespace AFPCrecer.LeadsCapture.Domain.Interface
{
   public interface ILeadsDomain
    {
        Task<object> RegistryLeadAsync(Leads lead);
    }
}

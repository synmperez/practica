﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace AFPCrecer.LeadsCapture.Services.ApiWeb
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((env,config) =>
            {
                var ambient = env.HostingEnvironment.EnvironmentName;
                
                config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                config.AddEnvironmentVariables();
                if (args != null)
                {
                    config.AddCommandLine(args);
                }
                  var currentConfing = config.Build();
     
                config.AddAzureKeyVault(currentConfing["Vault"],currentConfing["ClientId"],currentConfing["ClientSecret"]);
            })
                .UseStartup<Startup>();
    }
}

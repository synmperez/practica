﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AutoMapper;
using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Aplication.Main;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Cross.Mapper;
using AFPCrecer.LeadsCapture.Domain.Core;
using AFPCrecer.LeadsCapture.Domain.Entity;
using AFPCrecer.LeadsCapture.Domain.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Data;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using AFPCrecer.LeadsCapture.Infrastructure.Repository;
using System.Reflection;
using AFPCrecer.LeadsCapture.Cross.Wizard;

namespace AFPCrecer.LeadsCapture.Services.ApiWeb
{
    public class Startup
    {
        readonly string myPolicy = "policyApiLeadsCapture";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(MappinsProfile)));
            services.AddCors(options => options.AddPolicy(myPolicy,builder => builder.WithOrigins(Configuration["config:OriginCors"])
            .AllowAnyHeader()
            .AllowAnyMethod()));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => { options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver(); });


            services.AddApiVersioning(o => {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IConnectionFactory, ConnectionFactory>();
            services.AddScoped<ILeadAplication, LeadAplication>();
            services.AddScoped<ILeadsDomain, LeadsDomain>();
            services.AddScoped<IInvestorAplication, InvestorAplication>();
            services.AddScoped<IInvestorDomain, InvestorDomain>();
            services.AddScoped<IFinancialHealthAplication, FinancialHealthAplication>();
            services.AddScoped<IFinancialHealthDomain, FinancialHealthDomain>();
            services.AddScoped<IEncryptWizard, EncryptWizard>();
            services.AddScoped<ISendMailFactory, SendMailFactory>();
            services.AddScoped<ISendMailDomain, SendMailDomain>();
            services.AddScoped<ISendMailAplication, SendMailAplication>();
            services.AddScoped<IGeneratePDF, GeneratePDFWizard>();
            services.AddScoped<IDreamsEstimateAplication, DreamsEstimateAplication>();
            services.AddScoped<IDreamsEstimationDomain, DreamsEstimateDomain>();
            services.AddScoped<IRunStoreProcedureRepository, RunStoreProcedureRepository>();
            services.AddScoped<IPensionEstimateDomain, PensionEstimateDomain>();
            services.AddScoped<IPensionEstimateAplication, PensionEstimateAplication>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors(myPolicy);
           // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

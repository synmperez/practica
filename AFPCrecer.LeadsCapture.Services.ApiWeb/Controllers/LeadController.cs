﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Aplication.Interface;
using AFPCrecer.LeadsCapture.Cross.Wizard;
using AFPCrecer.LeadsCapture.Domain.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AFPCrecer.LeadsCapture.Services.ApiWeb.Controllers
{
    [Route("leads/[controller]")]
    [ApiController]
    public class LeadController : ControllerBase
    {
        private readonly ILeadAplication _leadAplication;
        private readonly IInvestorAplication _investorAplication;
        private readonly IFinancialHealthAplication _financialHealthAplication;
        private readonly ISendMailAplication _sendMailAplication;
        private readonly IDreamsEstimateAplication _dreamsEstimateAplication;
        private readonly IPensionEstimateAplication _pensionEstimateAplication;
        public LeadController(ILeadAplication leadAplication, IInvestorAplication investorAplication, 
            IFinancialHealthAplication financialHealthAplication, ISendMailAplication sendMailAplication,
            IDreamsEstimateAplication dreamsEstimateAplication, IPensionEstimateAplication pensionEstimateAplication)
        {
            _leadAplication = leadAplication;
            _investorAplication = investorAplication;
            _financialHealthAplication = financialHealthAplication;
            _sendMailAplication = sendMailAplication;
            _dreamsEstimateAplication = dreamsEstimateAplication;
            _pensionEstimateAplication = pensionEstimateAplication;
        }
        [HttpGet()]
        public ActionResult<string> Get()
        {
            return "version 1.0: 09-11-2019";
        }
        [HttpPost]

        public async Task<ActionResult<object>> Post([FromBody] Leads lead)
        {
            var response = await _leadAplication.RegistryLeadAsync(lead);

            return Ok(response);
        }

        [HttpPost("/leads/investor")]
        public async Task<ActionResult<object>> Post([FromBody] Investor investor)
        {
            var response = await _investorAplication.RegistryInvestorAsync(investor);

            return Ok(response);
        }

        [HttpPost("/leads/financialhealt")]
        public async Task<ActionResult<object>> Post([FromBody] FinancialHealth financial)
        {
            var response = await _financialHealthAplication.RegistryFinancialHealthStateAsync(financial);
            return Ok(response);
        }

        [HttpPost("/leads/sendmail")]
        public async Task<ActionResult<object>> Post([FromBody] MailRequest mail)
        {
            var response = await _sendMailAplication.GetMailInfo(mail);
            //string holder = "";
            //string response = GeneratePDFWizard.GenratePDF2(holder);
            return Ok(response);
        }

        [HttpPost("/leads/dreamsEstimate")]
        public async Task<ActionResult<object>> Post([FromBody] DreamEstimate dreams)
        {
            var response = await _dreamsEstimateAplication.RegistryDreamsEstimateAsync(dreams);
            return Ok(response);
        }

        [HttpPost("/leads/pensionEstimate")]
        public async Task<ActionResult<object>> Post([FromBody] PensionEstimate pension)
        {
            var response = await _pensionEstimateAplication.EstimatePensionAsync(pension);
            return Ok(response);
        }
    }
}
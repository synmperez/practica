﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AFPCrecer.LeadsCapture.Services.ApiWeb.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class SmokeController : ControllerBase
    {
        #region METODOS
        [HttpGet]
        public IActionResult Test() => Ok("Is running...");
        #endregion
    }
}

﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Infrastructure.Repository
{
    public class RunStoreProcedureRepository: IRunStoreProcedureRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public RunStoreProcedureRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<DataTable> RunStoreProcedureSQLServerAsync(string SP, string[,] parameters)
        {
            DataTable dt = new DataTable();
            using (SqlConnection sql = _connectionFactory.GetConnection)
            {
                using (SqlCommand cmd = new SqlCommand(SP, sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    for (int i = 0; i <= parameters.GetUpperBound(0); i++)
                    {
                        cmd.Parameters.Add(new SqlParameter(parameters[i, 0], parameters[i, 1]));
                    }
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        dt.Load(reader);
                        return dt;
                    }
                }

            }

        }
      
    }
}

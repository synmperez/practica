﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Aplication.Interface;
using Microsoft.AspNetCore.Cors;

namespace AFPCrecer.LeadsCapture.Services.Api.Controllers
{
    [Route("leads/[controller]")]
    [ApiController]
    public class LeadController : Controller
    {
        private readonly ILeadAplication _leadAplication;
        public LeadController(ILeadAplication leadAplication)
        {
            _leadAplication = leadAplication;
        }

        [HttpPost]
      //  [EnableCors("CorsPolicy")]
        public async Task<ActionResult<object>> Post([FromBody] LeadDTO lead)
        {
            var response = await _leadAplication.RegistryLeadAsync(lead);
            if (response.IsSuccess == "200")
                return Ok(response);
            return BadRequest(response.Message);
        }
    }
}
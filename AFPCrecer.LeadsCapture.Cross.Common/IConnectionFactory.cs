﻿using System.Data.SqlClient;
using System.Data;

namespace AFPCrecer.LeadsCapture.Cross.Common
{
    public interface IConnectionFactory
    {
        SqlConnection GetConnection { get; }
    }
}

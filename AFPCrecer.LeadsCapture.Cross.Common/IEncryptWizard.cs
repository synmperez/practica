﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AFPCrecer.LeadsCapture.Cross.Common
{
    public interface IEncryptWizard
    {
        string Encrypt(string plainText, byte[] key, byte[] iv);
        string Decrypt(string cipherText, byte[] key, byte[] iv);

    }
}

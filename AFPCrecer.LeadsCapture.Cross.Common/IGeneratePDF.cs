﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Cross.Common
{
    public interface IGeneratePDF
    {
        Task<string> GenratePDF(string html);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Cross.Common
{
    public interface ISendMailFactory
    {
        Task<bool> sendmail(List<string> email, string file);
    }
}

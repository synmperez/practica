﻿
namespace AFPCrecer.LeadsCapture.Cross.Common
{
    public class Response<T>
    {
        public T Data { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}

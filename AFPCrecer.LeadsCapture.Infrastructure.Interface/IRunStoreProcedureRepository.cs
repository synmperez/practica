﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Infrastructure.Interface
{
    public interface IRunStoreProcedureRepository
    {
        Task<DataTable> RunStoreProcedureSQLServerAsync(string SP, string[,] parameters);
    }
}

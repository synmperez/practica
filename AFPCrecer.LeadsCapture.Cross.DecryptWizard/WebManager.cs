﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace AFPCrecer.LeadsCapture.Cross.Wizard
{
    public class WebManager
    {        
        public static Tuple<int, string> PerformRequest(object data, List<string> headers, string target, string contentType, string method = "POST")
        {
            WebRequest tRequest = WebRequest.Create(target);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(target);
            for (int i = 0; i < headers.Count; i++)
            {
                tRequest.Headers.Add(headers[i]);
                request.Headers.Add(headers[i]);
            }

            string finalResult = "";
            string meth = method.ToLower();
            int scode = 0;

            if (meth == "post" || meth == "put" || meth == "patch")
            {
                tRequest.Method = method;
                tRequest.ContentType = contentType;
                tRequest.Timeout = 20000;

                var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (HttpWebResponse tResponse = (HttpWebResponse) tRequest.GetResponse())
                    {
                        scode = (int)tResponse.StatusCode;
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {                            
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                finalResult = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            else if (meth == "get")
            {
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            finalResult = reader.ReadToEnd();
                        }
                    }
                }
            }
            Tuple<int, string> holder = new Tuple<int, string>(scode, finalResult);
            return holder;
        }
    }
}

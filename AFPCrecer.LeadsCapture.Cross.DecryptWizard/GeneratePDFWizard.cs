﻿using iText.IO.Source;
using iText;
using System;
using System.Collections.Generic;
using System.Text;
using iText.Html2pdf;
using iText.Kernel.Pdf;
using iText.Layout;
using AFPCrecer.LeadsCapture.Cross.Wizard;
using iText.Layout.Element;
using System.Threading.Tasks;
using AFPCrecer.LeadsCapture.Cross.Common;
using System.IO;
using iText.Kernel.Geom;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace AFPCrecer.LeadsCapture.Cross.Wizard
{
    public class GeneratePDFWizard : IGeneratePDF
    {
        public async Task<string> GenratePDF(string html)
        {
            MemoryStream baos = new MemoryStream();
            PdfWriter writer = new PdfWriter(baos);
            PdfDocument doc = new PdfDocument(writer);
            ConverterProperties props = new ConverterProperties();
            doc.SetDefaultPageSize(PageSize.LETTER);
            HtmlConverter.ConvertToPdf(html, doc, props);

            byte[] pdfOutput = baos.ToArray();

            string holder = Convert.ToBase64String(pdfOutput);
            return holder;
            //MemoryStream baos = new MemoryStream();
            //HtmlConverter.ConvertToPdf(html, baos);

            //byte[] pdfOutput = baos.ToArray();

            //string holder = Convert.ToBase64String(pdfOutput);
            //return holder;
        }

        public static string GenratePDF2(string html)
        {
            MemoryStream baos = new MemoryStream();
            PdfWriter writer = new PdfWriter(baos);
            PdfDocument doc = new PdfDocument(writer);
            ConverterProperties props = new ConverterProperties();
            doc.SetDefaultPageSize(PageSize.LETTER);
            HtmlConverter.ConvertToPdf(html, doc, props);
            
            byte[] pdfOutput = baos.ToArray();

            string holder = Convert.ToBase64String(pdfOutput);
            return holder;
        }
    }
}

﻿using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Interface
{
    public interface IPensionEstimateAplication
    {
        Task<Response<object>> EstimatePensionAsync(PensionEstimate pensionEstimate);
    }
}

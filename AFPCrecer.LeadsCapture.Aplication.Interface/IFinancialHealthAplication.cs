﻿using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Cross.Common;
using AFPCrecer.LeadsCapture.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Interface
{
   public  interface IFinancialHealthAplication
    {
        Task<Response<object>> RegistryFinancialHealthStateAsync(FinancialHealth financialHealth);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Cross.Common;
using System.Threading.Tasks;
using AFPCrecer.LeadsCapture.Domain.Entity;

namespace AFPCrecer.LeadsCapture.Aplication.Interface
{
    public interface ILeadAplication
    {
        Task<Response<object>> RegistryLeadAsync(Leads lead);
    }
}

﻿using AFPCrecer.LeadsCapture.Aplication.DTO;
using AFPCrecer.LeadsCapture.Domain.Entity;
using System.Threading.Tasks;

namespace AFPCrecer.LeadsCapture.Aplication.Interface
{
    public interface ISendMailAplication
    {
        Task<object> GetMailInfo(MailRequest mailInfo);
    }
}
